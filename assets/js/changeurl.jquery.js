(function($){
    $.fn.changeUrl = function(options) {
        var default_options = {
            url: '#',
            delay: 1000,
            type:'thisWindow'
        };
        
        this.settings = $.extend({}, default_options, options);
        var self = this;
        
        var actualUrl = { url: window.location.href };
        $.chatBot.utils.saveCookie($.chatBot.cookieKeys.previousPage, actualUrl);
        
        var _chatState = { 
            windowOpen: true, 
            position: [
                'left', 
                'bottom'
            ]
        };

        $.chatBot.utils.saveCookie($.chatBot.cookieKeys.chatstate, _chatState);
        
        
            if(self.settings.type=='thisWindow'){
                setTimeout(function(){
                    window.location.href = self.settings.url;
                }, self.settings.delay);
            }
            if(self.settings.type=='newWindow'){
                var idObjects = $.chatBot.idObjects;
                var $conversation = $('#' + idObjects.chatConversation);
                var link='<p>La ventana se abrir&aacute; en una ventana emergente. Si esto no ocurre, haga '+
                         '<a href="'+self.settings.url+'" target="_blank">click aqu&iacute;</a></p>'
                
                $conversation.find('.message.agent:last').append(link)
                setTimeout(function(){
                    window.open(
                        self.settings.url,
                        '_blank'
                    );
                }, self.settings.delay);
            }
            
        
    };
})($aivo);