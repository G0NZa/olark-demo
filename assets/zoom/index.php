<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<title>jQuery Zoom Demo</title>
	<style>
		/* styles unrelated to zoom */
		* { border:0; margin:0; padding:0;}
		p { position:absolute; top:3px; right:28px; color:#555; font:bold 13px/1 sans-serif;}
                img.min{ /*max-height: 100%; */ max-width: 100%; margin: 0 auto;}
		/* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}
		
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }
                #ex1{ overflow: hidden;}
		#ex2 img:hover { cursor: url(grab.cur), default; }
		#ex2 img:active { cursor: url(grabbed.cur), default; }
		#loading{
			display: block;
			z-index: 99999;
    		position: absolute;
    		right: 160px;
    		top: 160px;
    	}
	</style>
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
	<script src='jquery.zoom.min.js'></script>
	<script>
		$(window).load(function(){
			$('#ex1').zoom();
                        $('#loading').fadeOut('slow');
			//$('#ex2').zoom({ on:'grab' });
			//$('#ex3').zoom({ on:'click' });			 
			//$('#ex4').zoom({ on:'toggle' });
		});
	</script>
</head>
<body>
    <img src="../image/loading2.gif" id="loading">
	<span class='zoom' id='ex1' style="width:355px; height: 400px;">

		<img src='<?php echo $_GET['img']?>' alt='Infografía' class="min"/>
	</span>
</body>
</html>
