(function($){
    $.fn.showPicZoom = function(options) {
        var self = this;
        
        var _options = {
            debug: false,
            urlImg: '',
            title: '&nbsp;',
            text: '',
            wrappers: {
                titleContainer: 'titleContainer',
                iframeWrapper: 'iframe-wrapper',
                iframeContainer: 'iframe-container'
            },
            back: function(){},
            openUrl: function(){}
        };
                
        this.settings = $.extend(true, _options, options);
        
        this.createIframe = function($container) {
            var _protocol = window.location.protocol;
            
            $.chatBot.debug('clickOpenUrl 123');

            $container.html('');
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();
            
            var wrapper = '<div id="' + self.settings.wrappers.faqsWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.faqsContainer + '">' +
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);

            var $container=$('#agentbot-chat-complement-container');
            
            $container.next('#agentbot-chat-footer').html('');
            
            //$container.next('#agentbot-chat-footer').append('<div id="agentbot-chat-download" title="Descargar"></div>');
            $container.next('#agentbot-chat-footer').append('<div id="agentbot-chat-share" title="Compartir en Facebook"></div>');

            var url=_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/zoom/?img='+self.settings.urlImg ;
            
            $.chatBot.debug(url);

            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ self.settings.title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){
                $.chatBot.debug('click back!');
                self.settings.back(this);
            });
                        
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-download').show();
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-download').click(function(){
                    $.chatBot.events.click, 
                    self.download(self.settings.urlImg)
            });
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-share').show();
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-share').click(function(){
                    $.chatBot.events.click, 
                    self.fbPostWall(self.settings.urlImg,self.settings.text)
            });

            self.settings.openUrl(url);
        };

        this.fbPostWall = function(url,text) {
            $.chatBot.debug('// fbPostWall');
            $.chatBot.debug('description: '+$(text).text());
            $.chatBot.debug('picture: '+url);
            FB.ui({
                method: 'feed',
                name: 'Buenos Aires Ciudad',
                description: $(text).text(),
                link: url,
                picture: url
            }, self.requestCallback);
        }
        this.download = function(img) {
            var _protocol = window.location.protocol;
            window.open(_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/download/?img='+img);
        }
                
        return $(this).each(function(){
            self.createIframe($(this));
        });
    };
})($aivo);