(function($){
    $.fn.facebookShare = function(options) {
        var self = this;
        
        var _options = {
            debug: false,
            idObject: '',
            params: {
                appID: null,
                message: '',
                name: '',
                description: '',
                link: '',
                picture: ''
            }
        };
        
        this.settings = $.extend(true, _options, options);
                
        this.createButton = function($container) {
            
            $.chatBot.debug('Create Button');
                        
            var button = '<div id="'+this.settings.idObject+'"></div>';

            var $wrapper = $(button).appendTo($container);
            $wrapper.bind($.chatBot.events.click, self.fbPostWall);
        };
        
        this.loadFacebook = function() {
            $.chatBot.debug('- Load addScripts');
            $.chatBot.debug('Adding Facebook Script.');
            
            /* Facebook API */
            $.ajaxSetup({ cache: true });
            $.getScript('//connect.facebook.net/es_ES/all.js', function(){
                $.chatBot.debug('- End addScripts');
                $.chatBot.debug('FB Init');
                FB.init({
                    appId      : self.settings.params.appID,
                    status     : true, // check login status
                    cookie     : true, // enable cookies to allow the server to access the session
                    xfbml      : true  // parse XFBML
                });
            });
            
        }
        this.fbPostWall = function() {
            FB.ui({
                method: 'feed',
                message: self.settings.params.message,
                name: self.settings.params.name,
                description: (self.settings.params.description),
                link: self.settings.params.link,
                picture: self.settings.params.picture
            }, self.requestCallback);
        }
        this.fbRecommend = function() {
            FB.ui({
                method: 'apprequests',
                message: self.settings.params.description
            });
        }
        
        function requestCallback(response) {
        // Handle callback here
        }
        
        return $(this).each(function(){
            if($.chatBot.settings.facebook.appID){
                self.loadFacebook();
                self.createButton($(this));
            }
        });
    };
})($aivo);