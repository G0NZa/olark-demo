(function($){
    $.fn.openUrl = function(options) {
        var self = this;
        
        var _options = {
            debug: false,
            url: '',
            width: 360,
            height: 405,
            title: '',
            wrappers: {
                titleContainer: 'titleContainer',
                iframeWrapper: 'iframe-wrapper',
                iframeContainer: 'iframe-container'
            },
            back: function(){}
        };
                
        this.settings = $.extend(true, _options, options);
        
        this.createIframe = function($container) {
            $.chatBot.debug('createPlayer');
            
            /* Clean container if it was previously setted up */
            $('#' + self.settings.wrappers.titleContainer).find('h2').html(self.settings.title);
            $container.html('');

            var wrapper = '<div id="' + self.settings.wrappers.iframeWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.iframeContainer + '">' +
                                '<iframe width="'+self.settings.width+'" height="'+self.settings.height+'" class="iframe" src="'+self.settings.url+'" frameborder="0"  scrolling="auto" seamless>' +
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);
            var title='&nbsp;';
            $('#' + self.settings.wrappers.titleContainer).html('');
            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){
                $.chatBot.debug('click back!');
                $.core.complements.showFaqsInit();
            });

            /* show up complement if it's hidden */
            if (parseInt($container.parent().css('width')) < 365) {
                $container.parent().animate({width: 365}, function(){
                    $.chatBot.utils.fixAxis($(this).parent());
                });
            }
        };
                
        return $(this).each(function(){
            self.createIframe($(this));
        });
    };
})($aivo);