<?php 

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type');
}


//TODO: if debug mode.
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');


require_once("includes/curl.php");
require_once("includes/class.phpmailer.php");

class ConversationMailerException extends Exception {}

class ConversationMailer{

	public function __construct($hash, $email, $options = array()) {
		$this->email = $email;
		$this->hash  = $hash;
		$this->url      = isset($options["url"])      ? $options["url"]      : "http://apibot.agentbot.net/REST/messages/";
		$this->subject  = isset($options["subject"])  ? $options["subject"]  : "Conversación";
		$this->from     = isset($options["from"])     ? $options["from"]     : "noreply@aivo.co";
		$this->fromName = isset($options["fromName"]) ? $options["fromName"] : "Agente Virtual";
	}

	public function send() {
		$curl = new curl(array('debug' => ""));
		$data = $curl->get($this->url, array("hash" => $this->hash));//Obtains conversation
		$result = json_decode($data); //data: array("text" => "...", "response" => "...")

		$bodyhtml = $this->generateHtml($result);

		$this->sendEmail($bodyhtml);
	}

	private function generateHtml($conversation) {
		ob_start();
		include 'template.php';
		return ob_get_clean();
	}

	private function sendEmail($bodyhtml)
	{
		$mail = new PHPMailer();
		$mail->Encoding = "base64";
		$mail->IsSMTP(); // enable SMTP
		
		//Para salir con gmail
		$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true;  // authentication enabled
		$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465; 
		$mail->Username = "development@aivo.co";  
		$mail->Password = "RaSMvPEq";
		
		$mail->IsHTML(true);
		$mail->AddAddress($this->email);
		$mail->SetFrom($this->from, utf8_decode($this->fromName));
		$mail->Subject = utf8_decode($this->subject);
		$mail->MsgHTML($bodyhtml);

		// send email or throw exception!
		if(!$mail->Send()) {
			//TODO:Show the mailer error?
			//echo "Mailer Error: " . $mail->ErrorInfo;
			throw new ConversationMailerException("Message could not be sent. <hr />" . $mail->ErrorInfo, 1);
		}
	}
}

$message = array();

if(isset($_GET["hash"], $_GET["email"])) {
	$hash  = $_GET["hash"];
	$email = $_GET["email"];

	$options = array();
	$options["subject"]  = "Tu conversación con el Asesor Online de Ciudad de Buenos Aires";
	$options["from"]     = "noreply@buenosaires.gob.ar";
	$options["fromName"] = "Asesor Online de Ciudad de Buenos Aires";

	$conversationMailer = new ConversationMailer($hash, $email, $options);

	try {
		$conversationMailer->send();
		$message["success"] = 1;
	}
	catch (ConversationMailerException $e) {
		$message["error"] = $e->getMessage();
	} 
}
else {
	$message["error"] = "Bad parameters";
}

$message = json_encode($message);

if (isset($_REQUEST['callback'])) {
    $contentType = "application/x-javascript";
    $response = $_REQUEST['callback'].'('.$message.')';
} else {
    $contentType = "application/json";
    $response = $message;
}

header('Content-type: '.$contentType);
echo $response;