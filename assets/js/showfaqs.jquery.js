(function($){
    $.fn.showFaqs = function(options) {
        var self = this;
        
        var _options = {
            inputText: '',
            wrappers: {
                faqsWrapper: 'faqsWrapper',
                faqsContainer: 'faqsContainer',
                titleContainer: 'titleContainer'
            },
            data: {},
            callback: function(){},
            back: function(){}
        };
        
        this.settings = $.extend(true, _options, options);
        
        this.createFaqs = function($container) {
            $.chatBot.debug('create FAQS');
            /* Clean container if it was previously setted up */
            $container.html('');
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();
            
            var wrapper = '<div id="' + self.settings.wrappers.faqsWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.faqsContainer + '">' +
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);

            var title = self.settings.data.shift();
            var items = '';
            $.each(self.settings.data, function(index, value){
                items += '<a href="javascript:void(0)" class="faqsAction">' + value + '</a>';
            });
            
            $('#' + self.settings.wrappers.faqsContainer).append($(items));
            
            $('#' + self.settings.wrappers.faqsContainer).find('.faqsAction').bind($.chatBot.events.click, self.onClick);
            
            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>' + title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){
                $.chatBot.debug('click back!');
                self.settings.back(this);
            });
            
            //Expand complement
            $.chatBot.utils.expandComplement();
        };
        
        this.onClick = function(e) {
            e.preventDefault();
            var $this = $(this);
            var $target = $(self.settings.inputText);
            $target.val($this.text());
            self.settings.callback.call(this);
        };
        
        return $(this).each(function(){
            self.createFaqs($(this));
        });
    };
})($aivo);