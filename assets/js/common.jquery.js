(function($){
    $.fn.disable = function() {
        $(this).attr('disabled', 'disabled');
    };
    
    $.fn.enable = function() {
        $(this).attr('disabled', false);
        $(this).focus();
    };
    
    $.chatBot = {};

    $.chatBot.idObjects = {
        chatMainWindow: 'agentbot-chat-main',
        chatOverlay: 'agentbot-overlay',
        chatMainWrapper: 'agentbot-chat-main-wrapper',
        chatConversationWrapper: 'agentbot-chat-wrapper',
        chatCloseButton: 'agentbot-chat-close',
        chatConversation: 'agentbot-chat-conversation',
        chatInput: 'agentbot-chat-message',
        chatSendButton: 'agentbot-chat-send',
        chatPrintButton: 'agentbot-chat-print',
        chatAgentBotLogo: 'agentbot-chat-logoagent',
        chatSendMailButton: 'agentbot-chat-send-mail',
        chatFbShareButton: 'agentbot-chat-FBshare',
        chatFbRecommendButton: 'agentbot-chat-FBrecommend',
        chatComplement: 'agentbot-chat-complement',
        chatComplementContainer: 'agentbot-chat-complement-container',
        chatComplementHeader: 'agentbot-chat-complement-header',
        chatComplementFooter: 'agentbot-chat-complement-footer',
        chatComplementClose: 'agentbot-complement-close',
        chatComplementOpen: 'agentbot-complement-open',
        chatAvatar: 'agentbot-chat-avatar',
        chatHeader: 'agentbot-chat-header',
        chatFooter: 'agentbot-chat-footer',
        chatFeedback: 'agentbot-chat-feedback',
        chatFeedbackEmail: 'agentbot-feedback-email',
        chatFeedbackSend: 'agentbot-feedback-send',
        chatBrandLogo: 'agentbot-chat-logo',
        chatLoading: 'agentbot-chat-loading',
        chatWelcomeContainer: 'agentbot-chat-welcome-container',
        chatWelcomeMessage: 'agentbot-chat-welcome-message',
        chatWelcomeTitle: 'agentbot-chat-welcome-title',
        chatWelcomeBotButton: 'agentbot-chat-welcome-bot-button',
        chatWelcomeChatButton: 'agentbot-chat-welcome-chat-button',
        chatWelcomeBot2Button: 'agentbot-chat-welcome-bot2-button',
        requestPhoneWindow: 'agentbot-chat-phone-window',
        inputPhone: 'agentbot-input-phone',
        inputEmail: 'agentbot-input-email',
        emailWindow: 'agentbot-chat-email-window',
        sendMailButton: 'agentbot-chat-send-mail-button',
        sendPhoneButton: 'agentbot-chat-send-phone-button',
        faqsContainer: 'agentbot-chat-faqs-container',
        videoWrapper: 'agentbot-chat-video-wrapper',
        faqsWrapper: 'agentbot-chat-faqs-wrapper',
        imageWrapper: 'agentbot-chat-image-wrapper',
        imageContainer: 'agentbot-chat-image-container',
        videoContainer: 'agentbot-chat-video-container',
        tabLauncher: 'agentbot-tab',
        overlay: 'agentbot-overlay',
        loginOptionWindow: 'agentbot-login-option-container',
        loginOption: 'agentbot-login-option',
        loginNameInput: 'agentbot-login-name',
        loginEmailInput: 'agentbot-login-email',
        loginDoneButton: 'agentbot-login-send',
        loginDoneButtonContainer: 'agentbot-login-send-container',
        startMessage: 'agentbot-start-message',
        chatAvatarList: 'agentbot-avatar-list',
        chatAvatarListButton: 'agentbot-avatar-list-button',
        chatAvatarContainer: 'agentbot-avatar-container',
        chatAvatarContainerScroll: 'agentbot-avatar-container-scroll'
    };
    
    $.chatBot.defaultSettings = {
        debug: false,
        customLauncher: [],
        /* overlay: true, */
        position: [], /* fixed bottom left right top */
        zIndex: 1,
        endPoint: 'http://api.agentbotjs',
        assetsEndPoint: 'localhost.agentbotjs',
        expirationTime: 43200, /* time when cookie must expire expressed in minutes. 30 days */
        changeUrlType: 'thisWindow',
        avatar: 'nico',
        sessionTime: 6,
        conversation_options: {
            answerID: 0,
            token: '',
            hash: Math.floor((Math.random()*1000000)+1),
            sentence: '',
            memory: '',
            email: '',
            extra: {}
        },
        text: {
            agent: 'Agent',
            user: 'Yo',
            writeMessage: 'Escrib&iacute; tu mensaje ac&aacute;:',
            helpUsToBeBetter: 'Ayudanos a mejorar: &iquest;te sirvi&oacute; la respuesta? ',
            welcomeTitle: 'Bienvenido a la asistencia virtual',
            welcomeMessage: 'Hola soy un bot, como te puedo ayudar?',
            yes: 'SI',
            no: 'NO',
            dontLike: 'No me gusta',
            missingInformation: 'Falto informaci&oacute;n',
            wrongAnswer: 'Respuesta incorrecta',
            thanksMessage: '&iexcl;Gracias!',
            send: 'Enviar',
            inputEmail: 'email'
        },
        showTab: true
    };
    
    $.chatBot.events = {
        openChatWindow: 'openChatWindow.aivo',
        closeChatWindow: 'closeChatWindow.aivo',
        minimizeChatWindow: 'minimizeChatWindow.aivo',
        click: 'click.aivo',
        keypress: 'keypress.aivo',
        keydown: 'keydown.aivo',
        mouseDown: 'mousedown.aivo',
        mouseUp: 'mouseup.aivo',
        mouseMove: 'mousemove.aivo',
        scroll: 'scroll.aivo',
        focus: 'focus.aivo',
        blur: 'blur.aivo'
    };

    $.chatBot.cookieKeys = {
        conversation: 'conversation_options',
        chatstate: 'chatState',
        previuosPage: 'previousPage',
        loginOption: 'loginOptions',
        avatar: 'avatar'
    };

    $.chatBot.timesOfDay = {
        afternoon: 'afternoon',
        morning: 'morning',
        night: 'night'
    };

    $.chatBot.browser = {
        msie: false,
        version: 0,
        detect: function() {
            if(navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                $.chatBot.browser.msie = true;
                $.chatBot.browser.version = RegExp.$1;
            }
        }
    };
    
    $.chatBot.mobile = {
        android: false,
        blackberry: false,
        ios: false,
        firefoxos: false,
        windows: false,
        any: false,
        detect: function() {
            $.chatBot.mobile.android = navigator.userAgent.match(/Android/i) ? true : false;
            $.chatBot.mobile.blackberry = navigator.userAgent.match(/BlackBerry/i) ? true : false;
            $.chatBot.mobile.ios = navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
            $.chatBot.mobile.firefoxos = navigator.userAgent.match(/Mobile/i) ? true : false;
            $.chatBot.mobile.windows = navigator.userAgent.match(/IEMobile/i) ? true : false;
            $.chatBot.mobile.any = (
                    $.chatBot.mobile.android ||
                    $.chatBot.mobile.blackberry ||
                    $.chatBot.mobile.ios ||
                    $.chatBot.mobile.firefoxos ||
                    $.chatBot.mobile.windows
            );
        }
    };
    
    $.chatBot.utils = {
        startLoadingRequest: function() {
            $('#' + $.chatBot.idObjects.chatLoading).show();
        },
        stopLoadingRequest: function() {
            $('#' + $.chatBot.idObjects.chatLoading).hide();
        },
        addMinutesToCurrentTime: function(minutes) {
            return this.addMinutes(new Date, minutes);
        },
        addMinutes: function(date, minutes) {
            return new Date(date.getTime() + minutes * 60000);
        },
        getTimezone: function() {
            var d = new Date();
            return - d.getTimezoneOffset() / 60;
        },
        getDiffMinutes: function(dateOne, dateTwo) {
            return parseInt((dateOne.getTime() - dateTwo.getTime()) / 1000 / 60);
        },
        getTimesOfDay: function() {
            //5 - 12am morning 12-20 afternoon night
            var d = new Date();
            var hour = d.getHours(); //Client Time
            var timeZone = $.chatBot.timesOfDay.night;
            
            if (hour >= 5 && hour < 12) {
                timeZone = $.chatBot.timesOfDay.morning;
            } else if (hour >= 12 && hour < 20) {
                timeZone = $.chatBot.timesOfDay.afternoon;
            }

            return timeZone;
        },
        centerInWindow: function($this) {
            var left = (window.innerWidth * 0.5) - ($this.width() * 0.5);
            var offsetY = (window.innerHeight * 0.5) - ($this.height() * 0.5);
            /* If position is setted to absolute we have to add the scroll offset */
            var top = 0;
            if ($this.css('position') === 'absolute') {
                top = $('body.agentbot').scrollTop() + offsetY;
            } else if ($this.css('position') === 'fixed') {
                top = offsetY;
            }
            
            $this.animate({left: left, top: top}, 100);
            
            return this;
        },
        scrollDown: function($element) {
            $element.scrollTop($element[0].scrollHeight);
        },
        getMaxZindex: function($element) {
            return Math.max.apply(null,$.map($element, function(e,n){
                if($(e).css('position') === 'absolute') return parseInt($(e).css('z-index'))||1;
                return 1;
                })
            );
        },
        saveCookie: function(key, value) {
            $.cookie(
                key,
                value,
                {
                    path: '/', 
                    expires: $.chatBot.utils.addMinutesToCurrentTime($.chatBot.settings.expirationTime)
                }
            );
        },
        removeCookie: function(key) {
            $.removeCookie(key, {path: '/'});
        },
        expandComplement: function() {
            var $mainWindow = $('#' + $.chatBot.idObjects.chatMainWindow);
            var $complementWindow = $('#' + $.chatBot.idObjects.chatComplement);
            var chatComplementMaxWidth = parseInt($complementWindow.css('max-width'));

            $complementWindow.animate({width: chatComplementMaxWidth},
                {
                    complete: function() {
                        $.chatBot.utils.fixAxis($mainWindow);
                    }
                }, 200);
            $('#' + $.chatBot.idObjects.chatFooter).find('div').show();
        },
        collapseComplement: function() {
            $('#' + $.chatBot.idObjects.chatFooter).find('div').hide();
        },
        fixAxis: function($obj) {
            /* This method will fix X position when the window is outside from the document */
            if ((parseInt($obj.css('left')) + $obj.width()) > $(window).width()) {
                var maxX = $(window).width() - $obj.width();
                $obj.animate({left: maxX});
            }
        },
        updateSession: function() {
            //var loginOption = $.cookie($.chatBot.cookieKeys.loginOption);
            //loginOption.expire = $.chatBot.utils.addMinutesToCurrentTime($.chatBot.settings.sessionTime);
            //$.chatBot.utils.saveCookie($.chatBot.cookieKeys.loginOption, loginOption);
            //$.chatBot.debug('----------- update session -----------');
            //$.chatBot.debug(new Date($.cookie($.chatBot.cookieKeys.loginOption).expire).toString());
        },
        isSessionExpired: function() {
            var currentTime = new Date();
            var expireTime = '';
            var diffTime = '';            
            var loginOption = $.cookie($.chatBot.cookieKeys.loginOption);

            if (loginOption !== null) {
                if ($.chatBot.settings.text.expire == '') {
                    return true;
                } else {
                    expireTime = new Date($.chatBot.settings.text.expire);
            
                    /* time to expire */
                    diffTime = Math.round($.chatBot.utils.getDiffMinutes(expireTime, currentTime));
                
                    return diffTime <= 0;
                }
            } else {
                return true;
            }
        },
        isValidEmail: function(email) { 
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    };
    
    $.chatBot.debug = function(message) {
        if (window.console && window.console.log && $.chatBot.settings.debug === true) {
            window.console.log(message);
        }
    };
})($aivo);
/* IE Fix, cross-domain request. */
(function(jQuery) {
    if ( window.XDomainRequest ) {
        jQuery.ajaxTransport(function( s ) {
            if ( s.crossDomain && s.async ) {
                if ( s.timeout ) {
                    s.xdrTimeout = s.timeout;
                    delete s.timeout;
                }
                var xdr;
                return {
                    send: function( _, complete ) {
                        function callback( status, statusText, responses, responseHeaders ) {
                            xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
                            xdr = undefined;
                            complete( status, statusText, responses, responseHeaders );
                        }
                        xdr = new XDomainRequest();
                        xdr.onload = function() {
                            callback( 200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType );
                        };
                        xdr.onerror = function() {
                            callback( 404, "Not Found" );
                        };
                        xdr.onprogress = jQuery.noop;
                        xdr.ontimeout = function() {
                            callback( 0, "timeout" );
                        };
                        xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
                        xdr.open( s.type, s.url );
                        xdr.send( ( s.hasContent && s.data ) || null );
                    },
                    abort: function() {
                        if ( xdr ) {
                            xdr.onerror = jQuery.noop;
                            xdr.abort();
                        }
                    }
                };
            }
        });
    }
})($aivo);