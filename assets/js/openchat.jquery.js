(function($){
    $.fn.openChat = function(options) {
        var self = this;
        
        var _options = {
            debug: false,
            url: '',
            width: '98%',
            height: '100%',
            params: {},
            iframeContainer: 'externalChatContainer',
            iframeId: 'externalChat',
            classname: 'externalChat'
        };
        
        this.settings = $.extend(true, _options, options);
        
        /* Replace variables and make url to Open Chat. */
        this.getUrl = function(){
            var url = self.settings.url;
            
            $.each(self.settings.params, function(index, value){
                url = url.replace('{' + index + '}', value);
            });
            
            $.chatBot.debug('url: ' + url);
            
            return url;
        };
        
        this.createIframe = function($container) {
            $.chatBot.debug('Create Iframe');
            
            var url = self.getUrl();
            
            var wrapper = '<div id="'+self.settings.iframeContainer+'" class="box-overlay"><iframe src="'+ url +'" id="' + self.settings.iframeId + '" class="'+self.settings.classname+'" /></div>';

            var $wrapper = $(wrapper).hide().appendTo($container);
            
            var $closebutton = $('<a href="javascript:void(0)" class="backToChat">VOLVER CON EL AGENTE</a>').hide();
            
            $closebutton.appendTo($container);
            
            $closebutton.bind($.chatBot.events.click, self.backToSofia);
            
            setTimeout(function() { 

                var $wrapper = $('#' + self.settings.iframeContainer);
                var $closeButton = $('.backToChat');
                $wrapper.fadeIn(); 
                $closeButton.fadeIn();

            }, 2000);
        };
        
        this.backToSofia = function(e) {
            e.preventDefault();
            $('#' + self.settings.iframeContainer).fadeOut(200,function(){
                $('#' + self.settings.iframeId).remove();
                $('.backToChat').remove();
                $('#' + self.settings.iframeContainer).remove();
            });
            $('#agentbot-avatar-list').fadeIn(200);
        };
        
        return $(this).each(function(){
            self.createIframe($(this));
        });
    };
})($aivo);