<?php
DEFINE('TMP_DIR', 'tmp');
DEFINE('ASSETS_DIR', 'assets');
DEFINE('CACHED_FILE', 'agentbot.jquery.js');

require_once('includes/jsmin-php/jsmin.php');

getSource();

function getSource() 
{
    /* This flag indicates if the bundle needs to regenerate */
    $output = "";
    $filename = ASSETS_DIR.'/'.TMP_DIR.'/'.CACHED_FILE;

    $files = array(
        'assets/js/jquery.1.9.js',
        'assets/js/json3.min.js',
        'assets/js/cookie.jquery.js',
        'assets/js/common.jquery.js',
        'assets/js/playvideo.jquery.js',
        'assets/js/qualify.jquery.js',
        'assets/js/showfaqs.jquery.js',
        'assets/js/showfaqsinit.jquery.js',
        'assets/js/showpic.jquery.js',
        'assets/js/showpiczoom.jquery.js',
        'assets/js/changeurl.jquery.js',
        'assets/js/openchat.jquery.js',
        'assets/js/openurl.jquery.js',
        'assets/js/facebook.share.jquery.js',
        'assets/js/agentbot.jquery.js'
    );

    /* Check directories */
    if(!file_exists(ASSETS_DIR. '/' .TMP_DIR)) {
        mkdir(ASSETS_DIR.'/'.TMP_DIR);
    }

    /* Determines if we have to create/update the jquery file. */
    if(!file_exists($filename) || 1==1) {
        /* Generate output and save file */
        foreach ($files as $file) {
            $output .= JSMin::minify(file_get_contents($file));
        }

        $fp = fopen($filename,'w');
        fwrite($fp, $output);
        fclose($fp);

    } else {
        $output = file_get_contents($filename);
    }

    return $output;
}