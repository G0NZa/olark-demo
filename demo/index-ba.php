   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" dir="ltr" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<meta property="og:site_name" content="Buenos Aires Ciudad - Gobierno de la Ciudad Autónoma de Buenos Aires" />
<link rel="shortcut icon" href="http://www.buenosaires.gob.ar/sites/gcaba/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta property="og:type" content="government" />
<link rel="canonical" href="http://www.buenosaires.gob.ar/" />
<meta name="description" content="Sitio oficial del Gobierno de la Ciudad de Buenos Aires, Argentina. Información general, tramites, servicios, agenda, mapas y aplicaciones." />
<meta property="og:url" content="http://www.buenosaires.gob.ar/" />
<link rel="shortlink" href="http://www.buenosaires.gob.ar/" />
<link rel="publisher" href="https://plus.google.com/+GobiernodelaCiudaddeBuenosAires/" />
<meta property="og:title" content="Buenos Aires Ciudad - Gobierno de la Ciudad Autónoma de Buenos Aires" />
<script type="text/javascript">  var OA_zones = {
    'Banner Home Grance' : 6,
    'Banner Home Peque' : 7,
    'Banner Noticias Post Fotos' : 8,
    'Banner tramites 300x206' : 9,
    'Banner tramites 300x89' : 10,
    'Banner comunas 300x206' : 11,
    'Banner Atención Ciudadana 300x300' : 12,
    'Banners Paseo de la historieta 300x300' : 13,
    'Banner Atencion Ciudadana 300x300' : 14,
    'Banner Atencion Observatorio 300x300' : 15,
    'Banner Comunas 300x300' : 16,
    'Banner Comunas bis 300x300' : 17,
    'Banner Trámites (columna derecha) 300x600' : 18 ,
    'Banner Modernización 300x600' : 19,
    'Banner Desarrollo Urbano 300x600' : 26,
    'Home Cultura' : 27,
    'Banner Home Tarjeta Unica' : 28,
    'Banner Subte' : 30,
    'Banner AGC' : 25,
    'Linea 102' : 29,
    'Banner Audiovideoteca' : 31,
    'Banner IVC' : 35,
    'Banner Copidis (superior)' : 33,
    'Banner Copidis (inferior)' : 34,
    'Banner central Red ETEV' : 36,
    'Banner derecho Red ETEV' : 37,
    'Banner derecho Salud' : 40,
    'Banner BAJoven' : 44,
    'Banner TrabajoBA' : 46,
    'Banner Deportes' : 48,
    'Banner Distritos' : 49,
    'Desarrollo Económico' : 50,
    'Banner Deportes 300 x 600' : 54,
    'Zona comodin 1' : 55,
    'Zona comodin 2' : 56,
    'Zona comodin 3' : 57,
    'Ciudad Verde' : 58,
    'Desarrollo Saludable' : 59,
    'Bienal Ba Joven' : 65,
    'Banner central cdnnya' : 41,
    'Banner derecho cdnnya' : 42,
    'Banner Central Copidis' : 32,
    'Banner Central Plan Microcentro' : 68,
    'Banner izquierdo Defensa del Consumidor' : 60,
    'Banner central Defensa del Consumidor' : 61,
    'Banner Central AIP' : 69,
    'Banner Central Ciudad Verde  2' : 62,
    'Banner central Habitat' : 63,
    'Banner derecha Habitat' : 64,
    'Banner central cdnnya' : 66,
    'Libre deuda' : 70
  }</script>
<script type="text/javascript" src="http://adn.buenosaires.gob.ar/www/delivery/spcjs.php?999=test"></script>
  <meta property="fb:admins" content="740472262"/>
  <title>Buenos Aires Ciudad - Gobierno de la Ciudad Autónoma de Buenos Aires |</title>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
  <meta name="google-site-verification" content="ejPgR-TlTOEbxbCTePBtnxgxjHlvqVfLzB-hmFINGuQ" />
  <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700&amp;mzia48" media="all" />
<style type="text/css" media="all">@import url("http://www.buenosaires.gob.ar/modules/system/system.base.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/system/system.menus.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/system/system.messages.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/system/system.theme.css?mzia48");</style>
<style type="text/css" media="all">@import url("http://www.buenosaires.gob.ar/modules/book/book.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/date/date_api/date.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/field/theme/field.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/node/node.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/poll/poll.css?mzia48");
@import url("http://www.buenosaires.gob.ar/modules/user/user.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/views/css/views.css?mzia48");</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
.panels-flexible-region {
  padding: 0;
}

.panels-flexible-region-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-region-inside-first {
  padding-left: 0;
}

.panels-flexible-region-inside-last {
  padding-right: 0;
}

.panels-flexible-column {
  padding: 0;
}

.panels-flexible-column-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-column-inside-first {
  padding-left: 0;
}

.panels-flexible-column-inside-last {
  padding-right: 0;
}

.panels-flexible-row {
  padding: 0 0 0.5em 0;
  margin: 0;
}

.panels-flexible-row-last {
  padding-bottom: 0;
}

.panels-flexible-column-new-main {
  float: left;
  width: 100.0000%;
}

.panels-flexible-new-inside {
  padding-right: 0px;
}

.panels-flexible-new {
  width: auto;
}

.panels-flexible-region-new-center {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-main-row-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-shortcuts {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-1-inside {
  padding-right: 0px;
}

.panels-flexible-column-new-3 {
  float: left;
  width: 50.0000%;
}

.panels-flexible-column-new-4 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-column-new-5 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-row-new-2-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-noticias_en_la_ciudad {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-6-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-agenda_cultural {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-7-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-sidebar {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-8-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-destacados {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-9-inside {
  padding-right: 0px;
}

.panels-flexible-region {
  padding: 0;
}

.panels-flexible-region-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-region-inside-first {
  padding-left: 0;
}

.panels-flexible-region-inside-last {
  padding-right: 0;
}

.panels-flexible-column {
  padding: 0;
}

.panels-flexible-column-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-column-inside-first {
  padding-left: 0;
}

.panels-flexible-column-inside-last {
  padding-right: 0;
}

.panels-flexible-row {
  padding: 0 0 0.5em 0;
  margin: 0;
}

.panels-flexible-row-last {
  padding-bottom: 0;
}

.panels-flexible-column-new-main {
  float: left;
  width: 100.0000%;
}

.panels-flexible-new-inside {
  padding-right: 0px;
}

.panels-flexible-new {
  width: auto;
}

.panels-flexible-region-new-center {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-main-row-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-shortcuts {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-1-inside {
  padding-right: 0px;
}

.panels-flexible-column-new-3 {
  float: left;
  width: 50.0000%;
}

.panels-flexible-column-new-4 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-column-new-5 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-row-new-2-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-noticias_en_la_ciudad {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-6-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-agenda_cultural {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-7-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-sidebar {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-8-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-destacados {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-9-inside {
  padding-right: 0px;
}

.panels-flexible-region {
  padding: 0;
}

.panels-flexible-region-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-region-inside-first {
  padding-left: 0;
}

.panels-flexible-region-inside-last {
  padding-right: 0;
}

.panels-flexible-column {
  padding: 0;
}

.panels-flexible-column-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-column-inside-first {
  padding-left: 0;
}

.panels-flexible-column-inside-last {
  padding-right: 0;
}

.panels-flexible-row {
  padding: 0 0 0.5em 0;
  margin: 0;
}

.panels-flexible-row-last {
  padding-bottom: 0;
}

.panels-flexible-column-new-main {
  float: left;
  width: 100.0000%;
}

.panels-flexible-new-inside {
  padding-right: 0px;
}

.panels-flexible-new {
  width: auto;
}

.panels-flexible-region-new-center {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-main-row-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-shortcuts {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-1-inside {
  padding-right: 0px;
}

.panels-flexible-column-new-3 {
  float: left;
  width: 50.0000%;
}

.panels-flexible-column-new-4 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-column-new-5 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-row-new-2-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-noticias_en_la_ciudad {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-6-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-agenda_cultural {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-7-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-sidebar {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-8-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-destacados {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-9-inside {
  padding-right: 0px;
}

.panels-flexible-region {
  padding: 0;
}

.panels-flexible-region-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-region-inside-first {
  padding-left: 0;
}

.panels-flexible-region-inside-last {
  padding-right: 0;
}

.panels-flexible-column {
  padding: 0;
}

.panels-flexible-column-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-column-inside-first {
  padding-left: 0;
}

.panels-flexible-column-inside-last {
  padding-right: 0;
}

.panels-flexible-row {
  padding: 0 0 0.5em 0;
  margin: 0;
}

.panels-flexible-row-last {
  padding-bottom: 0;
}

.panels-flexible-column-new-main {
  float: left;
  width: 100.0000%;
}

.panels-flexible-new-inside {
  padding-right: 0px;
}

.panels-flexible-new {
  width: auto;
}

.panels-flexible-region-new-center {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-main-row-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-shortcuts {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-1-inside {
  padding-right: 0px;
}

.panels-flexible-column-new-3 {
  float: left;
  width: 50.0000%;
}

.panels-flexible-column-new-4 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-column-new-5 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-row-new-2-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-noticias_en_la_ciudad {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-6-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-agenda_cultural {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-7-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-sidebar {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-8-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-destacados {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-9-inside {
  padding-right: 0px;
}

.panels-flexible-region {
  padding: 0;
}

.panels-flexible-region-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-region-inside-first {
  padding-left: 0;
}

.panels-flexible-region-inside-last {
  padding-right: 0;
}

.panels-flexible-column {
  padding: 0;
}

.panels-flexible-column-inside {
  padding-right: 0.5em;
  padding-left: 0.5em;
}

.panels-flexible-column-inside-first {
  padding-left: 0;
}

.panels-flexible-column-inside-last {
  padding-right: 0;
}

.panels-flexible-row {
  padding: 0 0 0.5em 0;
  margin: 0;
}

.panels-flexible-row-last {
  padding-bottom: 0;
}

.panels-flexible-column-new-main {
  float: left;
  width: 100.0000%;
}

.panels-flexible-new-inside {
  padding-right: 0px;
}

.panels-flexible-new {
  width: auto;
}

.panels-flexible-region-new-center {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-main-row-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-shortcuts {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-1-inside {
  padding-right: 0px;
}

.panels-flexible-column-new-3 {
  float: left;
  width: 50.0000%;
}

.panels-flexible-column-new-4 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-column-new-5 {
  float: left;
  width: 25.0000%;
}

.panels-flexible-row-new-2-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-noticias_en_la_ciudad {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-6-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-agenda_cultural {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-7-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-sidebar {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-8-inside {
  padding-right: 0px;
}

.panels-flexible-region-new-destacados {
  float: left;
  width: 100.0000%;
}

.panels-flexible-row-new-9-inside {
  padding-right: 0px;
}


/*]]>*/-->
</style>
<style type="text/css" media="all">@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/uniform.default.css?mzia48");</style>

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/ie/ie-6.css?mzia48" media="all" />
<![endif]-->

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/ie/ie-7.css?mzia48" media="all" />
<![endif]-->

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/ie/ie-8.css?mzia48" media="all" />
<![endif]-->

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/ie/ie-9.css?mzia48" media="all" />
<![endif]-->
<style type="text/css" media="all">@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/bootstrap.min.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/chosen/chosen/chosen.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/chosen/css/chosen-drupal.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/ctools/css/ctools.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/panels/css/panels.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/typogrify/typogrify.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/modules/panels/plugins/layouts/flexible/flexible.css?mzia48");</style>
<style type="text/css" media="all">@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/reset.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/fonts.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/base.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/layout.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/header_and_nav.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/search.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/sliders.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/shortcuts.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/custom_blocks.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/to_migrate_bootstrap.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/internas.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/footer.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/system.css?mzia48");
@import url("http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/css/opengov.css?mzia48");</style>
  <script type="text/javascript">
    // Begin Print drupal basepath in a js variable for future reference
    var drupal_theme_path="/sites/gcaba/themes/gcba";
    // End Print drupal basepath in a js variable for future reference
    // Begin Registered sliders walkaround
  	var registered_sliders=[];
	var registered_media_viewers=[];
	function onYouTubePlayerReady(playerId) {
		for(i in registered_sliders){			
		  registered_sliders[i].register(playerId);
		}
		for(i in registered_media_viewers){
		  registered_media_viewers[i].register(playerId);  
		}
	}
	function stateChangeCallback(state){
	// 0->finish, 1->playing, 2->paused, 3->buffering, 5-> quequed
		for(i in registered_sliders){
			var s=registered_sliders[i];
			switch(state){
				case 0:s.resetTimer();break;
				case 1:s.stopTimer();break;
				case 3:s.stopTimer();break;
			}
		}
	}
	// End Registered sliders walkaround
  </script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.min.js?v=1.4.4"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/drupal.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/jquery.cookie.js?v=1.0"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/jquery.form.js?v=2.52"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/ajax.js?v=7.12"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/contentanalysis/contentanalysis.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/contentoptimizer/contentoptimizer.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/files/languages/es_KAewl5UHWeRnhAYUiLIHSoBalXl0AXLRotNJ6E-lI8o.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/chosen/chosen/chosen.jquery.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/panels/js/panels.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/js/custom-autocomplete.js?v=7.12"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/js/main.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/js/jquery.uniform.min.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/js/bootstrap.min.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/misc/progress.js?v=7.12"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/themes/gcba/js/slider_bootstrap.js?mzia48"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"gcba","theme_token":"vLUrIuEkp7Qwv4EAFKCF465_MhqOaNglKAWtLJmlJnc","js":{"sites\/gcaba\/modules\/chosen\/chosen.js":1,"sites\/gcaba\/modules\/gcaba_miba_dashboard\/js\/miba-header-widget.js":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/jquery.cookie.js":1,"misc\/jquery.form.js":1,"misc\/ajax.js":1,"sites\/gcaba\/modules\/contentanalysis\/contentanalysis.js":1,"sites\/gcaba\/modules\/contentoptimizer\/contentoptimizer.js":1,"public:\/\/languages\/es_KAewl5UHWeRnhAYUiLIHSoBalXl0AXLRotNJ6E-lI8o.js":1,"sites\/gcaba\/modules\/chosen\/chosen\/chosen.jquery.js":1,"sites\/gcaba\/modules\/panels\/js\/panels.js":1,"misc\/autocomplete.js":1,"sites\/gcaba\/themes\/gcba\/js\/main.js":1,"sites\/gcaba\/themes\/gcba\/js\/jquery.uniform.min.js":1,"sites\/gcaba\/themes\/gcba\/js\/bootstrap.min.js":1,"misc\/progress.js":1,"sites\/gcaba\/themes\/gcba\/js\/slider_bootstrap.js":1},"css":{"http:\/\/fonts.googleapis.com\/css?family=Open+Sans:400italic,400,700":1,"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/book\/book.css":1,"sites\/gcaba\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/poll\/poll.css":1,"modules\/user\/user.css":1,"sites\/gcaba\/modules\/views\/css\/views.css":1,"0":1,"1":1,"2":1,"3":1,"4":1,"sites\/gcaba\/themes\/gcba\/css\/uniform.default.css":1,"sites\/gcaba\/themes\/gcba\/css\/ie\/ie-6.css":1,"sites\/gcaba\/themes\/gcba\/css\/ie\/ie-7.css":1,"sites\/gcaba\/themes\/gcba\/css\/ie\/ie-8.css":1,"sites\/gcaba\/themes\/gcba\/css\/ie\/ie-9.css":1,"sites\/gcaba\/themes\/gcba\/css\/bootstrap.min.css":1,"sites\/gcaba\/modules\/chosen\/chosen\/chosen.css":1,"sites\/gcaba\/modules\/chosen\/css\/chosen-drupal.css":1,"sites\/gcaba\/modules\/ctools\/css\/ctools.css":1,"sites\/gcaba\/modules\/panels\/css\/panels.css":1,"sites\/gcaba\/modules\/typogrify\/typogrify.css":1,"sites\/gcaba\/modules\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"sites\/gcaba\/themes\/gcba\/css\/reset.css":1,"sites\/gcaba\/themes\/gcba\/css\/fonts.css":1,"sites\/gcaba\/themes\/gcba\/css\/base.css":1,"sites\/gcaba\/themes\/gcba\/css\/layout.css":1,"sites\/gcaba\/themes\/gcba\/css\/header_and_nav.css":1,"sites\/gcaba\/themes\/gcba\/css\/search.css":1,"sites\/gcaba\/themes\/gcba\/css\/sliders.css":1,"sites\/gcaba\/themes\/gcba\/css\/shortcuts.css":1,"sites\/gcaba\/themes\/gcba\/css\/custom_blocks.css":1,"sites\/gcaba\/themes\/gcba\/css\/to_migrate_bootstrap.css":1,"sites\/gcaba\/themes\/gcba\/css\/internas.css":1,"sites\/gcaba\/themes\/gcba\/css\/footer.css":1,"sites\/gcaba\/themes\/gcba\/css\/system.css":1,"sites\/gcaba\/themes\/gcba\/css\/opengov.css":1}},"chosen":{"selector":"select:visible","minimum":20,"minimum_width":200,"search_contains":false,"placeholder_text_multiple":"Choose some options","placeholder_text_single":"Choose an option","no_results_text":"No results match"},"ajax":{"edit-submit--3":{"callback":"gcaba_suscripcion_add","wrapper":"gcaba_suscripcion_result","progress":{"type":"none","message":"\u003Cdiv id=\u0022gcaba_suscripcion_result\u0022 class=\u0022feedback form_loading\u0022\u003ECargando\u003C\/div\u003E"},"event":"mousedown","keypress":true,"prevent":"click","url":"\/system\/ajax","submit":{"_triggering_element_name":"op","_triggering_element_value":"Suscribirse"}}}});
//--><!]]>
</script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-10722425-1', 'buenosaires.gob.ar');
    ga('send', 'pageview');

  </script>	
</head>
<body class="html front not-logged-in no-sidebars page-homepage" >
<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 <!-- Facebook Ends--> 
  
    <!--
<link type="text/css" rel="stylesheet" media="screen" href="/sites/gcaba/themes/gcba/css/header_especial.css" />
-->
<script src="/sites/gcaba/themes/gcba/js/jquery.autopager-1.0.0.js"></script>
<div class=" gob_header">
	<div class=" container">
			<div class="row-fluid">
				<div class="span8">
					<a class="iso_ciudad grid_3 alpha" href="/"></a>
					<!--<div class="lema_ciudad"></div>-->
					
					
					
						
					<div class="clear"></div>
				</div>
				<div class="span4">
					<div class="search-box grid_9 omega">
					    <div class="region region-search-box">
    <div id="block-bweb-form" class="block block-bweb">
      <div class="content">
    <form action="/" method="post" id="bweb-block-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-keys">
 <input type="text" id="edit-keys--2" name="keys" value="" size="15" maxlength="128" class="form-text form-autocomplete" /><input type="hidden" id="edit-keys--2-autocomplete" value="http://www.buenosaires.gob.ar/ajax/bweb-autocomplete" disabled="disabled" class="autocomplete" />
</div>
<input type="hidden" name="facet_type" value="" />
<input type="hidden" name="form_build_id" value="form-I1UfYziE3EfBTQTE4_ypXu-J94YLFc0Uocf7RYToYTI" />
<input type="hidden" name="form_id" value="bweb_block_form" />
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Buscar" class="form-submit" /></div></div></form>  </div>
</div>  </div>
					</div>
				</div>
			</div>
	</div>
</div>
<div id="header" class="fullwidth main_nav">
	<div class="container">
		<div class="row-fluid">
			<div class="span12">
				<div class="section clearfix">
					  <div class="region region-header">
    <div id="block-system-main-menu" class="block block-system block-menu">
      <div class="content">
    <div class="menu"><ul><li class="first leaf"><a href="/" title="" class="active">Inicio</a></li>
<li class="expanded"><a href="/ciudad" title="">La Ciudad</a><div class="menu_wrapper"><div class="borders"><div class="menu"><ul><li class="first leaf"><a href="/ciudadlimpia">Ciudad Limpia</a></li>
<li class="leaf"><a href="http://agendacultural.buenosaires.gob.ar/" title="Agenda Cultural">Agenda Cultural</a></li>
<li class="leaf"><a href="http://bue.gob.ar/" title="Sitio de Turismo GCBA">Turismo</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/barrios/buscador/" title="Buscador de barrios ">Barrios</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/directorio/" title="Directorio de Establecimientos">Establecimientos</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/ciudadabierta" title="">Ciudad Abierta</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/radiociudad" title="">Radio Ciudad AM 1110</a></li>
<li class="leaf"><a href="/la2x4" title="">La 2x4</a></li>
<li class="leaf"><a href="/foto" title="">Buenos Aires Foto</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/audiovideoteca" title="">Audiovideoteca de Buenos Aires</a></li>
<li class="leaf"><a href="/contenido/movilidad" title="">Movilidad</a></li>
<li class="last leaf"><a href="/revista-xq" title="">Revista XQ</a></li>
</ul></div><div class="destacados"><div class="view view-destacaods view-id-destacaods view-display-id-destacados_en_menu view-dom-id-e4cfa4db18e5cba5af1955f1fe4d046e">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/ciudaddelpapa"><h3>Ciudad del Papa</h3></a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/subte"><h3>Subte</h3></a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://ecobici.buenosaires.gob.ar/"><h3>EcoBici</h3></a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://movilidad.buenosaires.gob.ar/mapa-corte-calles/"><h3>Mapa Cortes de Tránsito</h3></a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.ausa.com.ar/"><h3>Estado Autopistas</h3></a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://movilidad.buenosaires.gob.ar/"><h3>Movilidad</h3></a></span>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.bancociudad.com.ar/personas"><h3>Banco Ciudad</h3></a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div><div class="noticias"><div class="view view-noticias view-id-noticias view-display-id-noticias_en_menu view-dom-id-7b297217222daa20890933fa9eeb1f62">
        
  
  
  
  
  
  
  
  
</div><div class="clear"></div></div><div class="clear"></div></div></div></li>
<li class="expanded"><a href="/areasdegobierno" title="">Gobierno</a><div class="menu_wrapper"><div class="borders"><div class="menu"><ul><li class="first leaf"><a href="/jefedegobierno" title="">Jefe de Gobierno</a></li>
<li class="leaf"><a href="/vicejefatura" title="">Vicejefatura</a></li>
<li class="leaf"><a href="/jefaturadegabinete" title="">Jefatura de Gabinete</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/des_social/" title="">Desarrollo Social</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/educacion/" title="">Educación</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/copidis" title="">COPIDIS</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/salud/" title="">Salud</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/gobierno/" title="">Justicia y Seguridad</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/med_ambiente/" title="">Ambiente y Espacio Publico</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/cultura/" title="">Cultura</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/desarrolloeconomico" title="">Desarrollo Economico</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/desarrollourbano" title="">Desarrollo Urbano</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/hacienda/" title="">Hacienda</a></li>
<li class="leaf"><a href="/cope" title="">Consejo de Planeamiento Estratégico</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/secretariageneral" title="">Secretaría General</a></li>
<li class="leaf"><a href="/secretaria-de-medios" title="">Secretaría de Medios</a></li>
<li class="leaf"><a href="/atencionciudadana" title="">Secretaría de Gestión Comunal y Atención Ciudadana</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/leg_tecnica/home/" title="">Legal y Tecnica</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/com_social/" title="">Comunicación Social</a></li>
<li class="leaf"><a href="/modernizacion" title="">Modernización</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/agc" title="">Agencia Gubernamental de Control</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/procuracion" title="">Procuración General de la Ciudad</a></li>
<li class="leaf"><a href="/comunas" title="">Comunas</a></li>
<li class="leaf"><a href="/gobierno" title="">Ministerio de Gobierno</a></li>
<li class="last leaf"><a href="/cdnnya" title="">Consejo de los Derechos de Niñas, Niños, y Adolescentes</a></li>
</ul></div><div class="destacados"><div class="view view-destacaods view-id-destacaods view-display-id-destacados_en_menu view-dom-id-6dfaa43e7b402fbf03f69f71fee30f04">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://agendacultural.buenosaires.gob.ar"><h3>Agenda Cultural</h3></a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/sarmientoba"><h3>Plan S@rmiento</h3></a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/ciudadverde"><h3>Ciudad Verde</h3></a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://data.buenosaires.gob.ar/"><h3>Buenos Aires Data</h3></a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/redentodoestasvos"><h3>Red En todo estás vos</h3></a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div><div class="noticias"><div class="view view-noticias view-id-noticias view-display-id-noticias_en_menu view-dom-id-7b297217222daa20890933fa9eeb1f62">
        
  
  
  
  
  
  
  
  
</div><div class="clear"></div></div><div class="clear"></div></div></div></li>
<li class="expanded"><a href="http://www.buenosaires.gob.ar/atencionciudadana" title="">Atención Ciudadana</a><div class="menu_wrapper"><div class="borders"><div class="menu"><ul><li class="first collapsed"><a href="http://www.buenosaires.gob.ar/online/" title="tramites y servicios online">Bs As Online</a></li>
<li class="leaf"><a href="http://www.agip.gov.ar/web/index.htm" title="Rentas de la Ciudad">Rentas</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/jef_gabinete/atencion_ciudadana/def_consumidor/?menu_id=174" title="Atención y defensa al consumidor">Defensa al Consumidor</a></li>
<li class="leaf"><a href="/comunas" title="">Comunas</a></li>
<li class="leaf"><a href="http://www.agip.gob.ar/web/" title="ABL">ABL</a></li>
<li class="leaf"><a href="http://www.agip.gob.ar/web/" title="Patente">Patente</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/localizacion-expediente" title="">Localización de expediente</a></li>
<li class="leaf"><a href="/tramites/tad" title="">Trámites a Distancia</a></li>
<li class="last leaf"><a href="/turno-autopartes" title="">Turno para grabado de autopartes</a></li>
</ul></div><div class="destacados"><div class="view view-destacaods view-id-destacaods view-display-id-destacados_en_menu view-dom-id-8d1286398981ca216b82b1bd381de2e4">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/saca-tu-turno"><h3>Sacá tu turno</h3></a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites"><h3>Guía de trámites</h3></a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="https://reclamos.buenosaires.gob.ar/suaci/contacto"><h3>Hacé tu reclamo</h3></a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/consulta-de-infracciones"><h3>Consultá infracciones de tránsito</h3></a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/solicitud-web-tramites-de-matrimonios"><h3>Reservá sala de matrimonio</h3></a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://suaci.buenosaires.gob.ar/suaci/solicitudPartida"><h3>Solicitá partidas online</h3></a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div><div class="noticias"><div class="view view-noticias view-id-noticias view-display-id-noticias_en_menu view-dom-id-7b297217222daa20890933fa9eeb1f62">
        
  
  
  
  
  
  
  
  
</div><div class="clear"></div></div><div class="clear"></div></div></div></li>
<li class="expanded"><a href="http://boletinoficial.buenosaires.gob.ar" title="">Info Pública</a><div class="menu_wrapper"><div class="borders"><div class="menu"><ul><li class="first leaf"><a href="http://boletinoficial.buenosaires.gob.ar/apps/BO/front/index.php" title="">Boletín Oficial</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/leg_tecnica/sin/index.php?s=idn&amp;ids=ids&amp;menu_id=21259" title="">Normativa</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/organigrama/?menu_id=505" title="">Organigrama</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/organigrama/funcionarios/?menu_id=713" title="">Guía de Funcionarios</a></li>
<li class="leaf"><a href="http://data.buenosaires.gob.ar" title="">Gobierno Abierto - Catalogo</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/agcontrol/" title="">Agencia Gubernamental de Control</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/hacienda/compras/?menu_id=9411" title="">Compras y Contrataciones</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/hacienda/sis_estadistico/?menu_id=5867" title="">Estadística y Censos</a></li>
<li class="leaf"><a href="/elecciones2013" title="">Elecciones 2013</a></li>
<li class="leaf"><a href="http://www.legislatura.gov.ar/" title="">Legislatura CABA</a></li>
<li class="last leaf"><a href="http://padron.buenosaires.gob.ar/" title="">Padrón Electoral definitivo para extranjeros</a></li>
</ul></div><div class="destacados"><div class="view view-destacaods view-id-destacaods view-display-id-destacados_en_menu view-dom-id-2ce9be2b2092fbf89152c5c85c623a02">
        
  
  
  
  
  
  
  
  
</div></div><div class="noticias"><div class="view view-noticias view-id-noticias view-display-id-noticias_en_menu view-dom-id-7b297217222daa20890933fa9eeb1f62">
        
  
  
  
  
  
  
  
  
</div><div class="clear"></div></div><div class="clear"></div></div></div></li>
<li class="leaf"><a href="http://www.bue.gob.ar" title="">Turismo</a></li>
<li class="leaf"><a href="/noticias" title="">Noticias</a></li>
<li class="last leaf"><a href="/accesible" title="">Accesible</a></li>
</ul></div>  </div>
</div>  </div>
					<!-- Construir comportamiento dropdown de menus -->
					<script type="text/javascript">
					jQuery(function($){
						$('.menu .active').click(function(e){
							e.preventDefault();
						});
						$('.menu > ul > li.expanded').each(function(i){
							$(this).hover(
								function(e){
									if($(this).parent().parent().parent().hasClass('content')){
										$(this).children('.menu_wrapper').fadeIn(0);
										$(this).addClass('expanded_hover');
									}
								},
								function(e){
									$(this).children('.menu_wrapper').fadeOut(0);
									$(this).removeClass('expanded_hover');
								}
							);
						});
					}); 
					</script>
					<!-- End -->
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	</div> <!-- /.section, /#header -->


<div id="page-wrapper" class="fullwidth">
	<div id="page">
	  <div id="main-wrapper"><div id="main" class="clearfix">
	    <div id="content" class="column"><div class="section">
	      <a id="main-content"></a>
	      <div class="tabs"></div>	      	      	        <div class="region region-content">
    <div id="block-system-main" class="block block-system">
      <div class="content">
    <div class="panel-flexible panels-flexible-new clearfix" id="homepage">
<div class="panel-flexible-inside panels-flexible-new-inside">
<div class="panels-flexible-row panels-flexible-row-new-main-row panels-flexible-row-first clearfix container_12">
  <div class="inside panels-flexible-row-inside panels-flexible-row-new-main-row-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-new-center panels-flexible-region-first panels-flexible-region-last grid_12">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-center-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-views-noticias-slider-home" >
  
      
  
  <div class="pane-content">
    <div class="view view-noticias view-id-noticias view-display-id-slider_home view-dom-id-dca634604eb12acd367b37d768656d8b">
      
  
  
      <div class="view-content">
      <div id="area-slider" class="carousel slide" style="height:385px;overflow:hidden;">
  <ol class="carousel-indicators">
    	    <li data-target="#area-slider" data-slide-to="0"></li>
  		    <li data-target="#area-slider" data-slide-to="1"></li>
  		    <li data-target="#area-slider" data-slide-to="2"></li>
  		    <li data-target="#area-slider" data-slide-to="3"></li>
  	  </ol>
	<div class="carousel-inner">
							  <div class="item active">
		    	
			  <img typeof="foaf:Image" src="http://www.buenosaires.gob.ar/sites/gcaba/files/styles/noticias_slidergrande/public/field/image/mm_home_2.jpg" width="940" height="385" alt="El jefe de Gobierno porteño, Mauricio Macri, supervisó el avance de las obras de reconstrucción de la centenaria escuela técnica Nº5 María de los Remedios de Escalada de San Martín   Fotos: Maria Ines Ghiglione-NP/GCVA" title="El jefe de Gobierno porteño, Mauricio Macri, supervisó el avance de las obras de reconstrucción de la centenaria escuela técnica Nº5 María de los Remedios de Escalada de San Martín   Fotos: Maria Ines Ghiglione-NP/GCVA" />				<div class="carousel-caption">
					  	<h4><span class="field-content"><a href="/noticias/macri-superviso-la-obra-de-construccion-del-nuevo-edificio-de-una-escuela-tecnica-de-flores">Macri supervisó la construcción del nuevo edificio de la Escuela Técnica Número 5 de Flores</a></span></h4>
			 
					  	<p><a href="/noticias/macri-superviso-la-obra-de-construccion-del-nuevo-edificio-de-una-escuela-tecnica-de-flores">Albergará a más de 600 alumnos, tendrá dos patios y dos áreas para deportes y recreación, una de ellas en la terraza y la otra en el subsuelo. Estará lista para el comienzo del ciclo lectivo. Además, se realizará la restauración integral del inmueble histórico de la calle Alberdi.</a></p>
				
		</div>
		
			    		  </div>
							  <div class="item">
		    	
			  <img typeof="foaf:Image" src="http://www.buenosaires.gob.ar/sites/gcaba/files/styles/noticias_slidergrande/public/field/image/home_29.jpg" width="940" height="385" alt="Paula Cano tiene 37 años, es Maestra Especializada en Educación Inicial y estudiante del último año del Profesorado de Educación Primaria y forma parte de Vacaciones en la Escuela." title="Paula Cano tiene 37 años, es Maestra Especializada en Educación Inicial y estudiante del último año del Profesorado de Educación Primaria y forma parte de Vacaciones en la Escuela." />				<div class="carousel-caption">
					  	<h4><span class="field-content"><a href="/noticias/la-escuela-es-el-lugar-mas-noble-donde-los-chicos-deben-crecer">“La Escuela es el lugar más noble donde los chicos deben crecer”</a></span></h4>
			 
					  	<p><a href="/noticias/la-escuela-es-el-lugar-mas-noble-donde-los-chicos-deben-crecer">Así lo siente Paula Cano, docente del programa Vacaciones en la Escuela. Destacó la “contención” que brinda el colegio y el “aprendizaje constante” de los chicos y los profes. Las vivencias diarias de una madre que desde hace siete años es parte del programa del que participan más de 15 mil niños.</a></p>
				
		</div>
		
			    		  </div>
							  <div class="item">
		    	
			  <img typeof="foaf:Image" src="http://www.buenosaires.gob.ar/sites/gcaba/files/styles/noticias_slidergrande/public/field/image/empresas-da_1.jpg" width="940" height="385" alt="Ya funcionan 140 empresas en el Distrito Audiovisual. Foto: Distrito Audiovisual/GCBA." title="Ya funcionan 140 empresas en el Distrito Audiovisual. Foto: Distrito Audiovisual/GCBA." />				<div class="carousel-caption">
					  	<h4><span class="field-content"><a href="/noticias/ya-funcionan-140-empresas-en-el-distrito-audiovisual">Ya funcionan 140 empresas en el Distrito Audiovisual</a></span></h4>
			 
					  	<p><a href="/noticias/ya-funcionan-140-empresas-en-el-distrito-audiovisual">Abarca 550 hectáreas de manzanas de la Ciudad, entre los barrios de Chacarita, Villa Ortúzar, La Paternal, Palermo y Colegiales. Es un atractivo para los productores de cine y publicidad del mundo. En 2013 se rodaron en Buenos Aires 534 producciones y se estrenaron unas 148 películas nacionales. La industria moviliza fuertes inversiones, genera valor agregado, empleo calificado y divisas.</a></p>
				
		</div>
		
			    		  </div>
							  <div class="item">
		    	
			  <img typeof="foaf:Image" src="http://www.buenosaires.gob.ar/sites/gcaba/files/styles/noticias_slidergrande/public/field/image/sintesis.jpg" width="940" height="385" alt="La Ciudad en hechos: Desarrollo Económico. Foto: Desarrollo Económico/GCBA." title="La Ciudad en hechos: Desarrollo Económico. Foto: Desarrollo Económico/GCBA." />				<div class="carousel-caption">
					  	<h4><span class="field-content"><a href="/noticias/el-ano-en-hechos-desarrollo-economico">El año en hechos: Desarrollo Económico</a></span></h4>
			 
					  	<p><a href="/noticias/el-ano-en-hechos-desarrollo-economico">La Ciudad impulsa el crecimiento creativo de todos sus barrios y piensa el Buenos Aires del futuro para beneficio de todos sus ciudadanos. Así lo demuestran el desarrollo y el fomento a la creación de distritos de industrias y rubros: el Tecnológico, el Audiovisual, el Diseño y las Artes. Además, se suman las industrias editoriales con La Noche de las Librerías; el intercambio de conocimiento junto a Singularitu University, la Ciudad de Moda, la Misión a Silicon Valley, el Día del Emprendedor y el proyecto de una nueva terminal de ómnibus para dinamizar el sur de la Ciudad. </a></p>
				
		</div>
		
			    		  </div>
		  </div>
  <!-- Carousel nav -->
  <a class="carousel-control left" href="#area-slider" data-slide="prev">&lsaquo;</a>
  <a class="carousel-control right" href="#area-slider" data-slide="next">&rsaquo;</a>
</div>    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-views-4f49535a0c673e8c5e31bff0d5b4d58e" >
  
      
  
  <div class="pane-content">
    
<div class="view view-avisos-urgentes view-id-avisos_urgentes view-display-id-avisos_urgentes_general view-dom-id-328a9c1fe75897e50ff9f394b439aac6 urgente">
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><div class="aviso_urgente clearfix"><h5>calor</h5><p>Consejos y recomendaciones ante las altas temperaturas</p><p class="masinfo">+info</p>
<span class="link">http://www.buenosaires.gob.ar/noticias/consejos-y-recomendaciones-del-gobierno-porteno-ante-las-altas-temperaturas</div>
</div></span>  </div>  </div>
    </div>
    <script type="text/javascript">
	jQuery(function($){
		$(".urgente .aviso_urgente").each(function(i){
			$(this).click(function(e){
				location.href=$(this).children(".link").html();
			});
		});
	});
	</script>
</div>   </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-new-1 clearfix container_12">
  <div class="inside panels-flexible-row-inside panels-flexible-row-new-1-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-new-shortcuts panels-flexible-region-first panels-flexible-region-last grid_12">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-shortcuts-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-views-shortcuts-home-block" >
  
      
  
  <div class="pane-content">
    
<div class="view view-shortcuts-home view-id-shortcuts_home view-display-id-block view-dom-id-556ce4563f0b3ce83a1b6dbf5119cc99 grid_12 shortcuts alpha omega">
  <div class="shortcuts_top separador"></div>
<div class="views-row views-row-1 views-row-odd views-row-first alpha grid_3">
	<div class="canvas">
		<div class="circle grey ">
	<a href=http://www.buenosaires.gob.ar/tramites title="">
		<span>
			<em style="background-image:url('http://www.buenosaires.gob.ar/sites/gcaba/files/short-computadora.png');"></em>
		</span>
		<h3>Guía de Trámites</h3>
	</a>
</div>
<div class="views-field views-field-field-description">
    <div class="field-content"></div>
</div>	</div>
</div>
<div class="views-row views-row-2 views-row-even grid_3">
	<div class="canvas">
		<div class="circle grey ">
	<a href=http://mapa.buenosaires.gob.ar/ title="">
		<span>
			<em style="background-image:url('http://www.buenosaires.gob.ar/sites/gcaba/files/short-mapa.png');"></em>
		</span>
		<h3>Mapa interactivo</h3>
	</a>
</div>
<div class="views-field views-field-field-description">
    <div class="field-content"></div>
</div>	</div>
</div>
<div class="views-row views-row-3 views-row-odd grid_3">
	<div class="canvas">
		<div class="circle grey ">
	<a href=http://www.buenosaires.gob.ar/redentodoestasvos title="">
		<span>
			<em style="background-image:url('http://www.buenosaires.gob.ar/sites/gcaba/files/short-atencion.png');"></em>
		</span>
		<h3>Red En Todo Estás Vos</h3>
	</a>
</div>
<div class="views-field views-field-field-description">
    <div class="field-content"></div>
</div>	</div>
</div>
<div class="views-row views-row-4 views-row-even views-row-last omega grid_3">
	<div class="canvas">
		<div class="circle grey baverde">
	<a href=http://www.buenosaires.gob.ar/ciudadverde title="">
		<span>
			<em style="background-image:url('http://www.buenosaires.gob.ar/sites/gcaba/files/short-ciudadverde.png');"></em>
		</span>
		<h3>Ciudad Verde</h3>
	</a>
</div>
<div class="views-field views-field-field-description">
    <div class="field-content"></div>
</div>	</div>
</div>
	<div class="clear"></div>
	<div class="shortcuts_bottom separador"></div>
	<div class="clear"></div>  </div>
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-gcaba-ba147-chat147-button" >
  
      
  
  <div class="pane-content">
    <script type="text/javascript" src="../agent.php?token=2a19e25ce0aa68b385c93c723df1986e&position=center,top"></script>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-new-2 clearfix container_12">
  <div class="inside panels-flexible-row-inside panels-flexible-row-new-2-inside clearfix">
<div class="panels-flexible-column panels-flexible-column-new-3 panels-flexible-column-first grid_4 no_background_block agenda_cultural">
  <div class="inside panels-flexible-column-inside panels-flexible-column-new-3-inside panels-flexible-column-inside-first">
<div class="panels-flexible-region panels-flexible-region-new-noticias_en_la_ciudad panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-noticias_en_la_ciudad-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-gcaba-agenda-cultural-agendaws" >
  
      
  
  <div class="pane-content">
    
        <div class="pane-content">
        <div class="circle dark clearfix agenda-cultural">
            <span>
                <em></em>
            </span>
            <h3>Agenda Cultural</h3>
        </div>
        <div class="view view-agenda-cultural view-id-agenda_cultural view-display-id-agenda_cultural_home">
        <div class="view-content">
        
            <div class="views-row views-row-1 views-row-odd views-row">
            <div class="views-field views-field-title">
            <span class="field-content">
                <div class="thumb">
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/4º-temporada-de-crimen-en-la-munich/8934" title="4º temporada de Crimen en la Munich">
                        <img src="http://fotos.agendacultural.buenosaires.gob.ar/4º-temporada-de-crimen-en-la-munich/homethumbs/7b02-crimenenlamunich.jpg" alt="Imagen de 4º temporada de Crimen en la Munich">
                    </a>
                </div>
                <div class="content">
                <p>
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/4º-temporada-de-crimen-en-la-munich/8934" title="4º temporada de Crimen en la Munich">4º temporada de Crimen en la Munich</a>
                </p>
		<div class="resumenAgendaHome">
                    El 18 de enero se inicia la cuarta temporada consecutiva de ?Crimen en la Munich?, la original propuesta inter...
		</div>

                </div>
           <div class="clear"></div>
           </span>
           </div>
           </div>
        
            <div class="views-row views-row-2 views-row-even views-row">
            <div class="views-field views-field-title">
            <span class="field-content">
                <div class="thumb">
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/participá-del-sorteo-de-dos-bicis/8946" title="Participá del sorteo de dos bicis">
                        <img src="http://fotos.agendacultural.buenosaires.gob.ar/participá-del-sorteo-de-dos-bicis/homethumbs/f521-sorteo2.jpg" alt="Imagen de Participá del sorteo de dos bicis">
                    </a>
                </div>
                <div class="content">
                <p>
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/participá-del-sorteo-de-dos-bicis/8946" title="Participá del sorteo de dos bicis">Participá del sorteo de dos bicis</a>
                </p>
		<div class="resumenAgendaHome">
                    Te invitamos a participar de un sorteo de dos bicis para chicos ¡Dejanos tus datos en el link que figura más...
		</div>

                </div>
           <div class="clear"></div>
           </span>
           </div>
           </div>
        
            <div class="views-row views-row-3 views-row-odd views-row">
            <div class="views-field views-field-title">
            <span class="field-content">
                <div class="thumb">
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/vacaciones-para-chicos-en-el-distrito-de-diseño/8923" title="Vacaciones para chicos en el Distrito de Diseño">
                        <img src="http://fotos.agendacultural.buenosaires.gob.ar/vacaciones-para-chicos-en-el-distrito-de-diseño/homethumbs/5608-cmd.jpg" alt="Imagen de Vacaciones para chicos en el Distrito de Diseño">
                    </a>
                </div>
                <div class="content">
                <p>
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/vacaciones-para-chicos-en-el-distrito-de-diseño/8923" title="Vacaciones para chicos en el Distrito de Diseño">Vacaciones para chicos en el Distrito de Diseño</a>
                </p>
		<div class="resumenAgendaHome">
                    Juegoteca de Verano (enero y febrero) espacio de juegos para chicos de entre 6 y 12 años. Actividad gratuita ...
		</div>

                </div>
           <div class="clear"></div>
           </span>
           </div>
           </div>
        
            <div class="views-row views-row-4 views-row-even views-row">
            <div class="views-field views-field-title">
            <span class="field-content">
                <div class="thumb">
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/verano-en-el-recoleta/8905" title="Verano en el Recoleta">
                        <img src="http://fotos.agendacultural.buenosaires.gob.ar/verano-en-el-recoleta/homethumbs/75ea-laboticadeangel.jpg" alt="Imagen de Verano en el Recoleta">
                    </a>
                </div>
                <div class="content">
                <p>
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/verano-en-el-recoleta/8905" title="Verano en el Recoleta">Verano en el Recoleta</a>
                </p>
		<div class="resumenAgendaHome">
                    Este verano el Centro Cultural Recoleta te invita a disfrutar de una gran  programación de muestras de arte �...
		</div>

                </div>
           <div class="clear"></div>
           </span>
           </div>
           </div>
        
            <div class="views-row views-row-5 views-row-odd views-row">
            <div class="views-field views-field-title">
            <span class="field-content">
                <div class="thumb">
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/constitución-de-la-nación-argentina-ilustrada/8715" title="Constitución de la Nación Argentina Ilustrada">
                        <img src="http://fotos.agendacultural.buenosaires.gob.ar/constitución-de-la-nación-argentina-ilustrada/homethumbs/0577-constitucion.jpg" alt="Imagen de Constitución de la Nación Argentina Ilustrada">
                    </a>
                </div>
                <div class="content">
                <p>
                    <a target="_blank" href="http://agendacultural.buenosaires.gob.ar/evento/constitución-de-la-nación-argentina-ilustrada/8715" title="Constitución de la Nación Argentina Ilustrada">Constitución de la Nación Argentina Ilustrada</a>
                </p>
		<div class="resumenAgendaHome">
                    El Centro Cultural Recoleta alberga la muestra "Constitución de la Nación Argentina Ilustrada" de Pablo Teme...
		</div>

                </div>
           <div class="clear"></div>
           </span>
           </div>
           </div>
        </div></div></div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-column panels-flexible-column-new-4 grid_4 no_background_block noticias">
  <div class="inside panels-flexible-column-inside panels-flexible-column-new-4-inside">
<div class="panels-flexible-region panels-flexible-region-new-agenda_cultural panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-agenda_cultural-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-gcaba-static-blocks-tramites-mini-guia" >
  
      
  
  <div class="pane-content">
    <div class="circle dark clearfix tramites-consultados"><span><em></em></span><h3>Trámites y Servicios</h3></div><div class="mini_tramites">
		<h4>Turno para infractores</h4>
		<p>Solicitud de turnos para una audiencia con un controlador de la UAF</p>
		<h5>Paso 1</h5>
		<ul>
			<li class="presunto_infractor"><a href="http://apps.buenosaires.gov.ar/apps/turnos_online/paso1.php?tramite=74"><span>Presunto Infractor</span><br /><!--Lorem ipsum dolro set amet--></a></li>
			<li class="empresas"><a href="http://apps.buenosaires.gov.ar/apps/turnos_online/paso1.php?tramite=66"><span>Empresas</span><br /><!--Lorem ipsum dolro set amet--></a></li>
			<li class="comercios"><a href="http://apps.buenosaires.gov.ar/apps/turnos_online/paso1.php?tramite=67"><span>Comercios</span><br /><!--Lorem ipsum dolro set amet--></a></li>
		</ul>
        <div class="clear"></div>
    </div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-bweb-form-tramites search_tramites"  id="search-tramites-home">
  
      
  
  <div class="pane-content">
    <form action="/" method="post" id="bweb-tramites-block-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-keys">
 <input type="text" id="edit-keys" name="keys" value="" size="15" maxlength="128" class="form-text form-autocomplete" /><input type="hidden" id="edit-keys-autocomplete" value="http://www.buenosaires.gob.ar/ajax/bweb-autocomplete" disabled="disabled" class="autocomplete" />
</div>
<input type="hidden" name="form_build_id" value="form-GOWFitc_8oj6OpWtF7emPMtZwbQpCRLTIisReSoankw" />
<input type="hidden" name="form_id" value="bweb_tramites_block_form" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Buscar" class="form-submit" /></div></div></form>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-views-mas-consultados-block" >
  
        <h2 class="pane-title">Los más consultados</h2>
    
  
  <div class="pane-content">
    <div class="view view-mas-consultados view-id-mas_consultados view-display-id-block view-dom-id-3e667af8b7245de0f8297405eddc7b5d">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/localizacion-expediente" title=" Localización de expediente">Localización de expediente</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/libre-deuda-de-infracciones" title=" Libre Deuda de Infracciones">Libre Deuda de Infracciones</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/pago-voluntario-de-infracciones-de-transito" title=" Pago Voluntario de Infracciones de Tránsito">Pago Voluntario de Infracciones de Tránsito</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/dni-duplicado-u-otro-ejemplar" title=" Duplicado de DNI">Duplicado de DNI</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/renovacion-de-licencia-de-conducir" title=" Renovación de Licencia de Conducir">Renovación de Licencia de Conducir</a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/tramites/solicitud-web-tramites-de-matrimonios" title=" Solicitud trámites de Matrimonios">Solicitud trámites de Matrimonios</a></span>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="http://www.buenosaires.gob.ar/consulta-de-infracciones" title=" Consulta de Infracciones">Consulta de Infracciones</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-column panels-flexible-column-new-5 panels-flexible-column-last grid_4 banners">
  <div class="inside panels-flexible-column-inside panels-flexible-column-new-5-inside panels-flexible-column-inside-last">
<div class="panels-flexible-region panels-flexible-region-new-sidebar panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-sidebar-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-gcaba-openx-0" >
  
      
  
  <div class="pane-content">
    <script type='text/javascript'><!--// <![CDATA[
    OA_show('Banner Home Grance');
// ]]> --></script>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-gcaba-openx-1 home_banner" >
  
      
  
  <div class="pane-content">
    <script type='text/javascript'><!--// <![CDATA[
    OA_show('Banner Home Peque');
// ]]> --></script>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-new-9 panels-flexible-row-last clearfix container_12">
  <div class="inside panels-flexible-row-inside panels-flexible-row-new-9-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-region panels-flexible-region-new-destacados panels-flexible-region-first panels-flexible-region-last slider-destacados grid_12">
  <div class="inside panels-flexible-region-inside panels-flexible-region-new-destacados-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
  </div>
</div>
  </div>
</div>
</div>
</div>
  </div>
</div>  </div>
	      	    </div></div> <!-- /.section, /#content -->
			<div class="clear"></div>
	  </div></div> <!-- /#main, /#main-wrapper -->
	</div>
	<div class="clear"></div>
</div> <!-- /#page, /#page-wrapper -->
<div id="footer" class="footer fullwidth">
	<div class="section container_12">
		<div class="grid_3">
			  <div class="region region-footer-first">
    <div id="block-menu-menu-servicios-web" class="block block-menu">
    <h2>Servicios Web</h2>
    <div class="content">
    <div class="menu"><ul><li class="first leaf"><a href="http://mapa.buenosaires.gob.ar" title="">Mapa Buenos Aires</a></li>
<li class="leaf"><a href="https://reclamos.buenosaires.gob.ar/suaci/contacto" title="Sistema unico de atencion ciudadana">Reclamos / Denuncias / Solicitudes</a></li>
<li class="leaf"><a href="http://www.agip.gov.ar/web/ventanillas/aplicativos/pat-01.htm" title="">Patentes: Consulta e impresión de boletas</a></li>
<li class="leaf"><a href="http://www.agip.gov.ar/web/ventanillas/aplicativos/abl-2012.html" title="">ABL: Consulta e impresión de boletas</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/seguridad_justicia/justicia_trabajo/adm_faltas/sol_tramites/?menu_id=21282" title="Solicitud de turno ante Controlador">Infracciones de tránsito: Solicitud de turno</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/obr_publicas/lic_conducir/cambios_subportal_licencias.php?menu_id=182" title="Inicio del trámite de otorgamiento o renovación">Licencias para conducir: Inicio del trámite</a></li>
<li class="leaf"><a href="/aplicaciones-moviles/bacomollego" title="">BA Cómo Llego</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/aplicaciones-moviles/ba-subte" title="">BA Subte</a></li>
<li class="leaf"><a href="/aplicaciones-moviles/baecobici" title="">BA Mejor en Bici</a></li>
<li class="leaf"><a href="/aplicaciones-moviles/ba-cultural" title="">BA Cultural</a></li>
<li class="leaf"><a href="/aplicaciones-moviles/bamovil" title="">BA Movil</a></li>
<li class="last leaf"><a href="http://www.buenosaires.gob.ar/apps/bawifi/" title="">BA WiFi</a></li>
</ul></div>  </div>
</div>  </div>
			<div class="clear"></div>
		</div>
		<div class="grid_3">
			  <div class="region region-footer-second">
    <div id="block-menu-menu-reas-de-gobierno" class="block block-menu">
    <h2>Áreas de Gobierno</h2>
    <div class="content">
    <div class="menu"><ul><li class="first leaf"><a href="/jefedegobierno" title="">Jefe de Gobierno</a></li>
<li class="leaf"><a href="/vicejefatura" title="">Vicejefatura de Gobierno</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/jefaturadegabinete" title="">Jefatura de Gabinete</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/med_ambiente/" title="">Ambiente y Espacio Público</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/cultura/" title="">Cultura</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/desarrolloeconomico" title="">Desarrollo Economico</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/des_social/" title="">Desarrollo Social</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/desarrollourbano" title="">Desarrollo Urbano</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/educacion/" title="">Educacion</a></li>
<li class="leaf"><a href="/gobierno" title="">Gobierno</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/hacienda/" title="">Hacienda</a></li>
<li class="leaf"><a href="/modernizacion" title="">Modernización</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/gobierno/" title="">Justicia y Seguridad</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/salud/" title="">Salud</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/secretariageneral" title="">Secretaría General</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/com_social/" title="">Comunicación Social</a></li>
<li class="leaf"><a href="/secretaria-de-medios" title="">Secretaría de Medios</a></li>
<li class="leaf"><a href="/atencionciudadana" title="">Gestión Comunal y Atención Ciudadana</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/leg_tecnica/home/" title="">Legal y Tecnica</a></li>
<li class="leaf"><a href="/cdnnya" title="">Consejo de los Derechos de Niñas, Niños, y Adolescentes</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/procuracion" title="">Procuración General de la Ciudad</a></li>
<li class="leaf"><a href="/registrocivil" title="">Registro Civil</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/sindicatura" title="">Sindicatura General de la Ciudad</a></li>
<li class="last leaf"><a href="/subte" title="subte buenos aires">Subte</a></li>
</ul></div>  </div>
</div>  </div>
			<div class="clear"></div>
		</div>
		<div class="grid_3">
			  <div class="region region-footer-third">
    <div id="block-menu-menu-informaci-n-p-blica" class="block block-menu">
    <h2>Información Pública</h2>
    <div class="content">
    <div class="menu"><ul><li class="first leaf"><a href="http://boletinoficial.buenosaires.gob.ar" title="">Boletín Oficial</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/leg_tecnica/sin/index.php?s=idn&amp;ids=ids&amp;menu_id=21259" title="">Normativa</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/organigrama/?menu_id=505" title="">Organigrama</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/organigrama/funcionarios/?menu_id=713" title="">Guía de Funcionarios</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/areas/hacienda/compras/?menu_id=9411" title="">Compras y Contrataciones</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/avisos/" title="">Licitaciones y Llamados</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/hacienda/sis_estadistico/?menu_id=5867" title="">Estadística y Censos</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/areas/com_social/registros/?menu_id=18577" title="">Registros Publicos</a></li>
<li class="leaf"><a href="http://www.buenosaires.gov.ar/directorio/" title="">Establecimientos</a></li>
<li class="leaf"><a href="http://www.buenosaires.gob.ar/agc" title="">Agencia Gubernamental de Control</a></li>
<li class="leaf"><a href="http://www.legislatura.gov.ar/" title="Legislatura CABA">Legislatura CABA</a></li>
<li class="leaf"><a href="/elecciones2013" title="">Elecciones 2013</a></li>
<li class="leaf"><a href="/terminos-y-condiciones" title="">Términos y condiciones</a></li>
<li class="leaf"><a href="/privacidad" title="">Política de privacidad</a></li>
<li class="last leaf"><a href="/creditos" title="">Gestión - Créditos</a></li>
</ul></div>  </div>
</div>  </div>
			<div class="clear"></div>
		</div>


		
		<div class="grid_3">
			<div>
				<h2>Redes Sociales</h2>
				<ul class="social_footer">
					<li class="facebook"><a title="Facebook del Gobierno de la Ciudad de Buenos Aires" href="http://www.facebook.com/gcba">facebook</a></li>
					<li class="twitter"><a title="Twitter del Gobierno de la Ciudad de Buenos Aires" href="http://www.twitter.com/gcba">twitter</a></li>
					<li class="rss"><a title="RSS del Gobierno de la Ciudad de Buenos Aires" href="rss/noticias">rss</a></li>
				</ul>
			</div>
			<div class="clear"></div>
			  <div class="region region-footer-fourth">
    <div id="block-gcaba-forms-suscripcion-home" class="block block-gcaba-forms">
    <h2>Suscripción</h2>
    <div class="content">
    <form class="gcaba-suscripcion-form" action="/" method="post" id="gcaba-suscripcion-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-correo">
 <input onfocus="if(this.value == &quot;Correo&quot;) { this.value = &quot;&quot;; }" onblur="if(this.value == &quot;&quot;) { this.value = &quot;Correo&quot;; }" type="text" id="edit-correo" name="correo" value="Correo" size="60" maxlength="128" class="form-text required" />
</div>
<div class="form-item form-type-textfield form-item-celular">
 <input onfocus="if(this.value == &quot;Celular&quot;) { this.value = &quot;&quot;; }" onblur="if(this.value == &quot;&quot;) { this.value = &quot;Celular&quot;; }" type="text" id="edit-celular" name="celular" value="Celular" size="60" maxlength="128" class="form-text required" />
</div>
<div class="form-item form-type-select form-item-operador">
 <select id="edit-operador" name="operador" class="form-select"><option value="none">- Operador -</option><option value="claro">Claro</option><option value="movistar">Movistar</option><option value="personal">Personal</option></select>
</div>
<input type="submit" id="edit-submit--3" name="op" value="Suscribirse" class="form-submit" /><div id="gcaba_suscripcion_result" class="feedback"></div><input type="hidden" name="form_build_id" value="form-gv1wDbvo7XQxPOFI0LO37mfHKN9zGqsgUxx6dYL34d0" />
<input type="hidden" name="form_id" value="gcaba_suscripcion_form" />
</div></form>  </div>
</div>  </div>
			<div><a rel="license" href="http://creativecommons.org/licenses/by/2.5/ar/deed.es_ES"><img alt="Licencia de Creative Commons" style="border-width:0" src="			http://i.creativecommons.org/l/by/2.5/ar/88x31.png" /></a><br />Los contenidos de <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.buenosaires.	gob.ar" property="cc:attributionName" rel="cc:attributionURL">Gobierno de la Ciudad Autónoma de Buenos Aires</a> están licenciados bajo <a rel="license" href	="http://creativecommons.org/licenses/by/2.5/ar/deed.es_ES">Creative Commons Reconocimiento 2.5 Argentina License</a>
			</div>	
		</div>

		<div class="clear"></div>
		
	</div>
</div> <!-- /.section, /#footer -->

<div class="tel-util clearfix fullwidth">
	<div class="section container_12 clearfix">
		<div class="grid_3">
			<div class="circle emergencias">
				<span><em></em></span>
			</div>
			<h2>103</h2>
			<h3>Emergencias</h3>
		</div>
		
		<div class="grid_3">
			<div class="circle same">
				<span><em></em></span>
			</div>
			<h2>107</h2>
			<h3>SAME</h3>
		</div>
		
		<div class="grid_3">
			<div class="circle linea_social">
				<span><em></em></span>
			</div>
			<h2>108</h2>
			<h3>Linea Social</h3>
		</div>
		
		<div class="grid_3">
			<div class="circle atencion_ciudadana">
				<span><em></em></span>
			</div>
			<h2>147</h2>
			<h3>Atencion ciudadana</h3>
		</div>
		
		
	</div>
</div>

  <script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/chosen/chosen.js?mzia48"></script>
<script type="text/javascript" src="http://www.buenosaires.gob.ar/sites/gcaba/modules/gcaba_miba_dashboard/js/miba-header-widget.js?mzia48"></script>
   <!-- Begin Social scripts -->
   <!-- Twitter-->
   <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
  <!-- Gplus-->
  <script type="text/javascript">
  window.___gcfg = {lang: 'es-419'};
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
  </script>
  <!-- End Social Scripts -->
  <script>
  jQuery(function(){ jQuery("#footer select").uniform(); });
  </script>

<!-- Google Code for Buenos Aires -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<div style="display:none">
<!-- Google Code for Buenos Aires -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1035889050;
var google_conversion_label = "Ko39CKrn6wMQmtP57QM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1035889050/?value=0&amp;label=Ko39CKrn6wMQmtP57QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div>

</body>
</html>
