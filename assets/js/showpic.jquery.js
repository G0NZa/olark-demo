(function($){
    $.fn.showPic = function(options) {
        var self = this;
        
        var _options = {
            debug: false,
            url: '',
            wrappers: {
                imageWrapper: 'image-wrapper',
                imageContainer: 'image-container',
                image: 'image',
                titleContainer: 'titleContainer'
            },
            back: function(){}
        };
        
        this.settings = $.extend(true, _options, options);
        
        this.createPic = function($container) {
            $.chatBot.debug('createPIC');
            /* Clean container if it was previously setted up */
            $container.html('');
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();
                        
            var wrapper = '<div id="' + self.settings.wrappers.imageWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.imageContainer + '">' +
                                '<div id="' + self.settings.wrappers.image + '">' +
                                    '<img src="'+self.settings.url+'" width="350">' +
                                '</div>' + 
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);
            
            var title='&nbsp;';
            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){
                $.chatBot.debug('click back!');
                self.settings.back(this);
            });
            
            
            //Expand complement
            $.chatBot.utils.expandComplement();
        };
        
        return $(this).each(function(){
            self.createPic($(this));
        });
    };
})($aivo);