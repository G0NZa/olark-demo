<?php
DEFINE('TMP_DIR', 'tmp');
DEFINE('ASSETS_DIR', 'assets');
DEFINE('CACHED_FILE', 'agentbot.jquery.js');

//require_once('includes/jsmin-php/jsmin.php');

if (isset($_GET["token"])) {
    //get the last-modified-date of this very file
    $lastModified=filemtime(__FILE__);
    //get a unique hash of this file (etag)
    $etagFile = md5_file(__FILE__);
    //get the HTTP_IF_MODIFIED_SINCE header if set
    $ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
    //get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
    $etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

    //set last-modified header
    header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
    //set etag-header
    header("Etag: $etagFile");
    //make sure caching is turned on
    header('Cache-Control: public');

    //check if page has changed. If not, send 304 and exit
    if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
    {
           header("HTTP/1.1 304 Not Modified");
           exit;
    }
    
    header('Content-Type: application/javascript');
    
    $token = trim($_GET['token']);
    $position = (isset($_GET["position"]) && $_GET["position"] !== '') ? explode(",", $_GET["position"]) : array('left', 'bottom');
    $debug = (isset($_GET["debug"])) ? true : null;
    $tab = (isset($_GET["tab"]) && $_GET["tab"] !== '') ? $_GET['tab'] : null;
    $changeURL = (isset($_GET["changeURL"]) && $_GET["changeURL"] !== '') ? $_GET['changeURL'] : 'thisWindow';
    $embed = (isset($_GET["embed"]) && $_GET["embed"] !== '') ? $_GET['embed'] : null;
    $visible = (isset($_GET["visible"]))? true : null;
    $fbShare = (isset($_GET["fbShare"]) && $_GET["fbShare"] !== '') ? $_GET['fbShare'] : null;
    /*
     * Si viene el fbShare damo el ID de la app de fb, sino el ID para compartir desde el sitio de buenos aires
     */
    $fbAppID = (isset($_GET["fbShare"]) && $_GET["fbShare"] !== '') ? '383816095087476' : '250751251753038';

    if (isset($embed)) {
        $position = array('left', 'relative');
        $visible = true;
    }

?>
if (typeof $aivo == "undefined") {
<?php
    echo getSource();
    $avatar[] = array(
        'name'=>'Nico',
        'class'=>'nico',
        'welcomeMessage' => '&iexcl;Hola soy Nico! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Lau',
        'class'=>'lau',
        'welcomeMessage' => '&iexcl;Hola soy Lau! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Silvia',
        'class'=>'silvia',
        'welcomeMessage' => '&iexcl;Hola soy Silvia! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Jorge',
        'class'=>'jorge',
        'welcomeMessage' => '&iexcl;Hola soy Jorge! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Sebasti&aacute;n',
        'class'=>'sebastian',
        'welcomeMessage' => '&iexcl;Hola soy Sebasti&aacute;n! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Marta',
        'class'=>'marta',
        'welcomeMessage' => '&iexcl;Hola soy Marta! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatar[] = array(
        'name'=>'Alex',
        'class'=>'alex',
        'welcomeMessage' => '&iexcl;Hola soy Alex! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.'
    );
    $avatarSelect=rand(0,6);
    
    $subdir = !strpos(__FILE__, '/html/demo/') ? '' : '/buenosaires';
?>
(function($){
    $(document).ready(function(){
        $('body').chatBot({
            <?php
                if (isset($debug)) {
                    echo 'debug: true,';
                }
                if (isset($tab)) {
                    echo 'showTab: '.$tab.',';
                }
                if (isset($fbShare)) {
                    echo 'fbShare: '.$fbShare.',';
                }
                if (isset($embed)) {
                    echo 'embed:  \'' . $embed . '\',';
                }
                if (isset($visible)) {
                    echo 'alwaysVisible: ' . $visible . ',';
                }
                if (isset($changeURL)) {
                    echo 'changeUrlType: "'.$changeURL.'",';
                }
            ?>
            startMessage: '0',
            endPoint: 'https://apibot.agentbot.net',
            assetsEndPoint: '<?= $_SERVER['HTTP_HOST'] . $subdir?>/assets',
            conversation_options: {
                token: '<?php echo $_GET["token"]; ?>'
            },
            position: <?php echo json_encode($position); ?>,
            avatar: '<?php echo $avatar[$avatarSelect]['class']; ?>',
            text: {
                agent: '<?php echo $avatar[$avatarSelect]['name']; ?>',
                welcomeTitle: 'Gobierno de la Ciudad de Buenos Aires',
                welcomeMessage: '<?php echo $avatar[$avatarSelect]['welcomeMessage']; ?>',
                tramiteMessage: '&iquest;En qu&eacute; puedo ayudarte?',
                escuelasMessage: '&iexcl;Hola soy <?php echo $avatar[$avatarSelect]['name']; ?>! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad.',
                conditionalGreeting: {
                    morning: 'Buenos d&iacute;as',
                    afternoon: 'Buenas tardes',
                    night: 'Buenas noches'
                },
                buttons: {
                    tramite: 'Informaci&oacute;n sobre tr&aacute;mites',
                    reclamo: 'Turnos, reclamos y otras consultas',
                    escuelas: 'Inscripci&oacute;n a escuelas'
                },
                derivation: 'Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias.',
                inputEmail: 'ingrese un email',
                expire: ''
            },
            facebook: {
                appID: '<?php echo $fbAppID; ?>',
                message: 'Chate&aacute; con un operador online del Gobierno de la Ciudad',
                name: 'Chate&aacute; con un operador online del Gobierno de la Ciudad',
                description: ('Nico y Lau te ayudan a resolver tus consultas sobre tr&aacute;mites y reclamos.'),
                link: 'https://apps.facebook.com/buenosairesbot/',
                picture: 'https://buenosaires.agentbot.net/assets/image/<?php echo $avatar[$avatarSelect]['class']; ?>.png'
            }
        });
    });
})($aivo);
}
<?php
} else {
    //getSource();
}

function getSource() 
{
    $output = '';
    $filename = ASSETS_DIR.'/'.TMP_DIR.'/'.CACHED_FILE;
    if(file_exists($filename)) {
        $output = file_get_contents($filename);
    }
    return $output;
}