(function($){
    $.fn.qualify = function(options) {
        var self = this;
        var default_options = {
            debug: false,
            endPoint: "localhost.agentbotjs",
            text: {
                helpUsToBeBetter: 'Ayudanos a mejorar: &iquest;te sirvi&oacute; la respuesta? ',
                yes: 'SI',
                no: 'NO',
                dontLike: 'No me gusta',
                missingInformation: 'Falto informaci&oacute;n',
                wrongAnswer: 'Respuesta incorrecta',
                thanksMessage: '&iexcl;Gracias!'
            },
            qualifyId: ''
        };
        
        var qualifyConstants = {
            yes: 1,
            no: 2,
            dontLike: 4,
            missingInformation: 5,
            wrongAnswer: 6
        };
        
        this.settings = $.extend({}, default_options, options);
        
        var template = '<span class="qualify-answer" id="qualify-' + this.settings.qualifyId + '">' + this.settings.text.helpUsToBeBetter  +
                        '<span><a data-qualifyid="'+ this.settings.qualifyId +'" data-qtype="'+ qualifyConstants.yes +'">' +  this.settings.text.yes + '</a></span>' +
                        '<span><a data-qualifyid="'+ this.settings.qualifyId +'" data-qtype="'+ qualifyConstants.no +'">' +  this.settings.text.no + '</a>' +
                        '<span class="dontLike">' +
                            '<ul>' + 
//                                '<li><a data-qualifyid="'+ this.settings.qualifyId +'" data-qtype="'+ qualifyConstants.dontLike +'">' +  this.settings.text.dontLike + '</a></li>' +
                                '<li><a data-qualifyid="'+ this.settings.qualifyId +'" data-qtype="'+ qualifyConstants.missingInformation +'">' +  this.settings.text.missingInformation + '</a></li>' +
                                '<li><a data-qualifyid="'+ this.settings.qualifyId +'" data-qtype="'+ qualifyConstants.wrongAnswer +'">' +  this.settings.text.wrongAnswer + '</a></li>' +
                            '</ul>' +
                        '</span></span>' + 
                       '</span>';

        this.addListeners = function(qualifyId) {
            $('#qualify-' + qualifyId + ' a').bind($.chatBot.events.click, self.qualifyHandler);
        };
        
        this.qualifyHandler = function(e) {
            var $this = $(this);
            var qualifyId = $this.data('qualifyid');
            var qualifyType = $this.data('qtype');

            $.chatBot.debug('idQualify ' + qualifyId + ' type : ' + qualifyType);

            var $contentQualify = null;
            if (qualifyType === qualifyConstants.yes) {
                
                //First Parent span
                //Second Parent span.qualify-answer
                $contentQualify = $this.parent().parent();
                self.thanksForQualify($contentQualify);

            } else if (qualifyType === qualifyConstants.no) {
                
                var $bubble = $this.parent().find('.dontLike');
                $bubble.fadeIn();
                setTimeout(
                    function() {
                        if ($bubble.is(':visible')) {
                            $bubble.fadeOut();
                        }
                    }, 
                    10000);
                //First Parent span
                //Second Parent span.qualify-answer
                $contentQualify = $bubble.parent().parent();

            } else { //dontLike, missingInformation, wrongAnswer
                
                //First Parent li
                //Second Parent lu
                //Third Parent span.dontLike
                var $bubble = $this.parent().parent().parent();
                $bubble.fadeOut();

                //First Parent span
                //Second Parent span.qualify-answer
                $contentQualify = $bubble.parent().parent();
                self.thanksForQualify($contentQualify);
            }

            self.sendQualify(qualifyId, qualifyType);
        };
        
        this.thanksForQualify = function($container) {
            $container.fadeOut(function(){
                $container.find('a').each(function(){
                    $(this).remove();
                    $.chatBot.debug('Removing: ' + $(this).html() + ' Button');
                });
                
                $container.html('<em>' + self.settings.text.thanksMessage + '</em>');
                $container.fadeIn(function(){
                    $.chatBot.utils.scrollDown($container.parent());

                    setTimeout(function(){
                        $container.slideUp(100);
                    }, 2000);
                });
            });
        };
        
        this.sendQualify = function(id, type) {
            $.ajax({
                url: self.settings.endPoint + '/REST/qualify/',
                data: {
                    message: id,
                    qualifyID: type
                },
                type: 'GET',
                contentType: "application/json",
                dataType: 'jsonp',
                success: function(data) {
                    $.chatBot.utils.stopLoadingRequest();
                    $.chatBot.debug('Qualification sent. ' + JSON.stringify(data));
                    if(data.bypass){
                        $aivo.core.processAgentMessage({
                            answer:data.bypass.bypassText,
                            answerID:0,
                            error:'',
                            extra:{message:0},
                            hasPattern:0,
                            memory:null,
                            patternID:0,
                            suggested:null
                            
                        });
                        $aivo.core.complements.openChat();
                    }
                },
                error: function(e) {
                    $.chatBot.utils.stopLoadingRequest();
                    $.chatBot.debug('Qualification failed: ' + e);
                }
            });
        };
        
        return $(this).each(function(){
            $(this).append($(template));
            self.addListeners(self.settings.qualifyId);
        });
    };
})($aivo);