(function($){
    $.fn.showFaqsInit = function(options) {
        var self = this;
        
        var _options = {
            inputText: '',
            wrappers: {
                faqsWrapper: 'faqsWrapper',
                faqsContainer: 'faqsContainer',
                titleContainer: 'titleContainer'
            },
            data: {},
            callback: function(){},
            openUrl: function(){}
        };
        
        this.settings = $.extend(true, _options, options);
        
        this.createFaqsInit = function($container) {
            $.chatBot.debug('create FAQS');
            $container.next('#agentbot-chat-footer').find('div').hide();
            /* Clean container if it was previously setted up */
            $container.html('');
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();

            
            if($.chatBot.settings.chatType && $.chatBot.settings.chatType == 3 && !$.chatBot.settings.defaultFaqs) {
                var wrapper = '<div id="' + self.settings.wrappers.faqsWrapper + '">' + 
                                '<div id="' + self.settings.wrappers.faqsContainer + '" class="init">' +
                                    /* '<h3>TR&Aacute;MITES</h3>' + */
                                    '<h3>&nbsp;</h3>' +
                                    '<ol>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">&iquest;Qu&eacute; pasos debo seguir para hacer la inscripci&oacute;n online a escuelas?</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">&iquest;Cu&aacute;ndo es la inscripci&oacute;n online a escuelas?</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">&iquest;A qui&eacute;n est&aacute; destinado el sistema de inscripci&oacute;n en l&iacute;nea a escuelas?</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">&iquest;C&oacute;mo confirmo la inscripci&oacute;n online a la escuela?</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">&iquest;El sistema de inscripci&oacute;n en l&iacute;nea a la Escuela P&uacute;blica me sirve para inscribirme a escuelas privadas?</a></li>' +
                                    '</ol>' +
                                    
                                    '<ul class="extraBtn">' +
                                        '<li><a href="http://www.buenosaires.gob.ar/saca-tu-turno" target="_blank" class="turnos">Sac&aacute; tu<br>turno</a></li>' +
                                        '<li><a href="javascript:void(0)" class="tramites">Tr&aacute;mites<br>ilustrados</a></li>' +
                                        '<li><a href="http://www.buenosaires.gob.ar/tramites" target="_blank" class="guia">Gu&iacute;a de<br>tr&aacute;mites</a></li>' +
                                    '</ul>' +
                                '</div>' +
                              '</div>';
                $(wrapper).appendTo($container);
                var title='Los m&aacute;s consultados';
                $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ title +'</h2>');
                $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){$.chatBot.settings.defaultFaqs=true;;self.createFaqsInit($container)});
            }
            else {
                var wrapper = '<div id="' + self.settings.wrappers.faqsWrapper + '">' + 
                                '<div id="' + self.settings.wrappers.faqsContainer + '" class="init">' +
                                    '<h3>TR&Aacute;MITES</h3>' +
                                    '<ol>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Renovaci&oacute;n de Licencia de conducir</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Infracciones Online</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Pago voluntario de infracciones de tr&aacute;nsito</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Solicitud de partidas</a></li>' +
                                    '</ol>' +
                                    '<h3>RECLAMOS</h3>' +
                                    '<ol>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Solicitud de Retiro de Restos de Obras o Demoliciones Domiciliarias H/ 500kg</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Luminarias Apagadas</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Retiro de Restos de Verdes de Jardines o Inmuebles</a></li>' +
                                        '<li><a class="faqsAction" href="javascript:void(0)">Solicitud de Retiro de Residuos Voluminosos</a></li>' +
                                    '</ol>' +
                                    '<ul class="extraBtn">' +
                                        '<li><a href="http://www.buenosaires.gob.ar/saca-tu-turno" target="_blank" class="turnos">Sac&aacute; tu<br>turno</a></li>' +
                                        '<li><a href="javascript:void(0)" class="tramites">Tr&aacute;mites<br>ilustrados</a></li>' +
                                        '<li><a href="http://www.buenosaires.gob.ar/tramites" target="_blank" class="guia">Gu&iacute;a de<br>tr&aacute;mites</a></li>' +
                                    '</ul>' +
                                '</div>' +
                              '</div>';
                $(wrapper).appendTo($container);
                var title='Los m&aacute;s consultados';
                $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle">'+ title +'</h2>');
            }
            $('#' + self.settings.wrappers.faqsContainer).find('.faqsAction').bind($.chatBot.events.click, self.onClick);
            $('#' + self.settings.wrappers.faqsContainer).find('.tramites').bind($.chatBot.events.click, self.viewTramites);
            


            
            /* Expand complement */
            $.chatBot.utils.expandComplement();
        };
        
        this.onClick = function(e) {
            e.preventDefault();
            var $this = $(this);
            var $target = $(self.settings.inputText);
            $target.val($this.text());
            self.settings.callback.call(this);
        };
        
        this.clickOpenUrl = function(e) {
            var _protocol = window.location.protocol;
            
            $.chatBot.debug('clickOpenUrl');
            e.preventDefault();
            var $container=$('#agentbot-chat-complement-container');
            var $this = $(this);
            
            $container.next('#agentbot-chat-footer').html('');
            $container.next('#agentbot-chat-footer').html('<div id="agentbot-chat-download" title="Descargar"></div><div id="agentbot-chat-share" title="Compartir en Facebook"></div>');
                        
            var url=_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/zoom/?img='+_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/image/infografia/'+$this.attr('rel');
            
            $('#' + self.settings.wrappers.titleContainer + ' h2.faqsTitle').html('<a class="back"></a>'+ $this.text());
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){self.createFaqsInit($container)});
            
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-download').show();
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-download').click(function(){
                    $.chatBot.events.click, 
                    self.download($this.attr('rel'))
            });
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-share').show();
            $container.next('#agentbot-chat-footer').find('div#agentbot-chat-share').click(function(){
                    $.chatBot.events.click, 
                    self.fbPostWall(_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/image/infografia/'+$this.attr('rel'),$this.text())
            });
            
            self.settings.openUrl(url);
        };
        
        this.fbPostWall = function(url,text) {
            $.chatBot.debug('// fbPostWall');
            $.chatBot.debug('name: '+text);
            $.chatBot.debug('picture: '+url);
            FB.ui({
                method: 'feed',
                name: text,
                caption: 'infografía',
                description: 'Buenos Aires Ciudad',
                link: url,
                picture: url
            }, self.requestCallback);
        };
        
        this.download = function(img) {
            var _protocol = window.location.protocol;
            window.open(_protocol + '//' + $.chatBot.settings.assetsEndPoint + '/download/?img='+img);
        };
        
        this.viewTramites = function(e) {
            var $container=$('#agentbot-chat-complement-container');
            e.preventDefault();
            $.chatBot.debug('View Tramites');
            /* Clean container if it was previously setted up */
            $container.html('');
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();
            
            var wrapper = '<div id="' + self.settings.wrappers.faqsWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.faqsContainer + '" class="init">' +
                                '<ul class="tramitesBtn">' +
                                    '<li><a class="faqsAction matrimonio" href="javascript:void(0)" rel="tramite_de_matrimonio.jpg">Reserva de sala para matrimonio</a></li>' +
                                    '<li><a class="faqsAction microcentro" href="javascript:void(0)" rel="ingreso_a_microcentro.jpg">Permiso de ingreso a microcentro</a></li>' +
                                    '<li><a class="faqsAction DNInuevo" href="javascript:void(0)" rel="duplicado_dni.jpg">DNI nuevo ejemplar</a></li>' +
                                    '<li><a class="faqsAction DNIdomicilio" href="javascript:void(0)" rel="cambio_de_domicilio.jpg">DNI - cambio de domicilio</a></li>' +
                                    '<li><a class="faqsAction infracciones" href="javascript:void(0)" rel="infracciones.jpg">Consulta y pago de infracciones online</a></li>' +
                                    '<li><a class="faqsAction nacimiento" href="javascript:void(0)" rel="inscripcion_de_nacimiento.jpg">Inscripci&oacute;n del nacimiento</a></li>' +
                                    '<li><a class="faqsAction otorgamientoLicencia" href="javascript:void(0)" rel="otorgamiento_de_licencia.jpg">Otorgamiento de licencia de conducir</a></li>' +
                                    '<li><a class="faqsAction partidas" href="javascript:void(0)" rel="partidas.jpg">Solicitud de partidas online</a></li>' +
                                    '<li><a class="faqsAction renovacionLicencia" href="javascript:void(0)" rel="renovacion_de_licencia.jpg">Renovaci&oacute;n de licencia de conducir</a></li>' +
                                '</ul>' +
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);
            var title='Tr&aacute;mites ilustrados';
            $('#' + self.settings.wrappers.faqsContainer).find('.faqsAction').bind($.chatBot.events.click, self.clickOpenUrl);
            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){self.createFaqsInit($container)});
        };  
        
        return $(this).each(function(){
            self.createFaqsInit($(this));
        });
    };
})($aivo);