<?php
header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Gobierno de la Ciudad de Buenos Aires</title>
        <style type="text/css">
            <!--
            html, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, sub, sup, tt, var, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, center, u, i {
                border: medium none;
                font-family: inherit;
                font-style: normal;
                font-weight: normal;
                margin: 0;
                outline: medium none;
                padding: 0;
            }
            .main {
                margin: 10px 0 0;
                overflow: hidden;
                width: 100%;
                z-index: -2;
                background: #FFF;
            }
            
            #chat-embebbed{
                border: none;
                width: 725px;
                height: 550px;
                margin: 0 auto;
                position: relative;
            }
            #chat-embebbed #agentbot-chat-main{
                position: relative;
            }
            body {
                margin: 0;
            }
            -->
        </style>
        <script type="text/javascript" src="../agent.php?token=f6efecd6e5f4d08c210f7f009f0af0b2&embed=chat-embebbed&tab=false&fbShare=true&welcomeTitle=Bienvenido al chat del Gobierno de la Ciudad de Buenos Aires"></script>
        
    </head>
    <body>        
        <div class="main">
            <div id="chat-embebbed" class="facebook"></div>
        </div>
        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-52216113-1', 'agentbot.net');
            ga('send', 'pageview');

          </script>
    </body>
</html>
