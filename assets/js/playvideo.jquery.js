(function($){
    $.fn.playVideo = function(options) {
        var self = this;
        
        var _youtubeOptions = {
            debug: false,
            autoPlay: false,
            videoCode: '',
            width: 332,
            height: 300,
            wrappers: {
                titleContainer: 'titleContainer',
                videoWrapper: 'video-wrapper',
                videoContainer: 'video-container',
                player: 'aivo_player'
            },
            back: function(){}
        };
        
        var player;
        
        this.settings = $.extend(true, _youtubeOptions, options);
        
        this.createPlayer = function($container) {
            $.chatBot.debug('createPlayer');
            /* Clean container if it was previously setted up */
            $('#' + self.settings.wrappers.titleContainer).find('h2').remove();
            $container.html('');
            $container.unbind($.chatBot.events.closeChatWindow);

            var wrapper = '<div id="' + self.settings.wrappers.videoWrapper + '">' + 
                            '<div id="' + self.settings.wrappers.videoContainer + '">' +
                                '<div id="' + self.settings.wrappers.player + '"></div>' + 
                            '</div>' +
                          '</div>';

            $(wrapper).appendTo($container);

            player = new YT.Player(self.settings.wrappers.player, {
                height: self.settings.height,
                width: self.settings.width,
                videoId: self.settings.videoCode,
                events: {
                  'onReady': self.onPlayerReady
                  //'onStateChange': self.onPlayerStateChange
                }
             });
             
            $container.bind($.chatBot.events.closeChatWindow, self.stopVideo);
            var title='Video';
            $('#' + self.settings.wrappers.titleContainer).append('<h2 class="faqsTitle"><a class="back"></a>'+ title +'</h2>');
            $('#' + self.settings.wrappers.titleContainer).find('.back').click(function(){
                $.chatBot.debug('click back!');
                self.settings.back(this);
            });
            
            //Expand complement
            $.chatBot.utils.expandComplement();
        };
        
        this.onPlayerReady = function(e) {
            if (self.settings.autoPlay === true) {
                e.target.playVideo();
            }
        };
        
        this.stopVideo = function(e) {
            $.chatBot.debug('stopVideo()');
            player.stopVideo();
        };
        
        return $(this).each(function(){
            self.createPlayer($(this));
        });
    };
})($aivo);