(function($){
    "use strict";    
    $.fn.chatBot = function(options){
        var $this = $(this);
        var self = this;
        var dataMessage = {'interval': 2000, 'showTime': 5000, 'numberRepeats': 1};
        var countRepeats = dataMessage.numberRepeats;
        var tabSpeed = 200; // slide effect speed
        var intervalHandler;
        var sessionTime = '';

        /* Center Tab */
        var topMax = $(window).height() - 515;
        topMax = topMax > 0 ? topMax:0;
        topMax = ($(window).height() / 2) > 514 ? ($(window).height() / 2) : topMax;
        
        $.chatBot.settings = $.extend(true, {}, $.chatBot.defaultSettings, options);
        $.chatBot.settings.chatType = 1;
        
        var idObjects = $.chatBot.idObjects;
        
        sessionTime = $.chatBot.settings.sessionTime;
        
        /* Facebook API */
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/es_ES/all.js', function(){
            $.chatBot.debug('- End addScripts');
            $.chatBot.debug('FB Init: '+$.chatBot.settings.facebook.appID);
            FB.init({
                appId      : $.chatBot.settings.facebook.appID, /* localhost: 251107335039482 */
                status     : true, /*check login status*/
                cookie     : true, /*enable cookies to allow the server to access the session*/
                xfbml      : true  /*parse XFBML*/
            });

        });
        
        var classObjects = {
            launcher: 'launcher',
            box: 'box-overlay',
            boxContainer: 'box-container',
            loadingMore: 'loadingMore',
            close: 'close',
            button: 'button',
            avatarItem: 'agentbot-avatar-item'
        };
        
        this.chatState = {
            windowOpen: false
        };

        this.dragging = false;
        
        var chatAvatars = '';            
       
        chatAvatars = {
            "nico": {
                "options": {
                    "avatarClass": "nico",
                    "name": "Nico",
                    "welcomeMessage": "&iexcl;Hola soy Nico! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Nico! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "lau": {
                "options": {
                    "avatarClass": "lau",
                    "name": "Lau",
                    "welcomeMessage": "&iexcl;Hola soy Lau! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Lau! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "silvia": {
                "options": {
                    "avatarClass": "silvia",
                    "name": "Silvia",
                    "welcomeMessage": "&iexcl;Hola soy Silvia! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Silvia! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "jorge": {
                "options": {
                    "avatarClass": "jorge",
                    "name": "Jorge",
                    "welcomeMessage": "&iexcl;Hola soy Jorge! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Jorge! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "sebastian": {
                "options": {
                    "avatarClass": "sebastian",
                    "name": "Sebasti&aacute;n",
                    "welcomeMessage": "&iexcl;Hola soy Sebasti&aacute;n! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Sebasti&aacute;n! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "marta": {
                "options": {
                    "avatarClass": "marta",
                    "name": "Marta",
                    "welcomeMessage": "&iexcl;Hola soy Marta! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Marta! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            },
            "alex": {
                "options": {
                    "avatarClass": "alex",
                    "name": "Alex",
                    "welcomeMessage": "&iexcl;Hola soy Alex! soy un agente virtual del Gobierno de la Ciudad, estoy para atender a tus consultas.",
                    "escuelasMessage": "&iexcl;Hola soy Alex! Ac&aacute; podr&aacute;s realizar las consultas que desees sobre el nuevo sistema de inscripci&oacute;n online para ingresantes a todos los niveles educativos de gesti&oacute;n estatal de la Ciudad."
                },
                "derivation": "Por favor, aguard&aacute; mientras te contactamos con un agente online. Muchas gracias."
            }
        };
        
        /* Cambia el avatar actual por uno a elección */
        this.derivateAvatar = function(avatar) {
            $.chatBot.debug('derivateAvatar');
            var agentData = '';
            var avatarClass = $.chatBot.settings.avatar;
            
            var $conversation = $('#' + idObjects.chatConversation);
            var $element = self.buildMessage('&iquest;Te gustan las Bicis? &iexcl;Conoc&eacute; a Nico que es un experto en bicis!', 'agent');
            $conversation.append($element);
            $.chatBot.utils.scrollDown($conversation);
            
            $.each(chatAvatars, function(i) {
                if (chatAvatars[i].options.name === avatar) {
                    agentData = chatAvatars[i];
                    
                    $('.' + classObjects.avatarItem).removeClass('selected');
                    $('#avatar-' + avatar).addClass('selected');
                    $.chatBot.debug(avatar);
                    /* Update facebook picture */
                    $.chatBot.settings.facebook.picture = $.chatBot.settings.assetsEndPoint + '/image/' + chatAvatars[i].options.avatarClass + '.png';
                }
            });
            
            /* Actualizo los datos del bot */
            $.chatBot.settings.text.agent = agentData.options.name;
            $.chatBot.settings.avatar = agentData.options.avatarClass;
            $.chatBot.settings.text.escuelasMessage = agentData.options.escuelasMessage;
            $.chatBot.settings.text.derivation = agentData.derivation;
            
            /* Actualizo las cookies del bot */
            $.chatBot.utils.saveCookie($.chatBot.cookieKeys.avatar, agentData.options);
            
            /* Change logo */
            $('#' + idObjects.chatMainWindow).removeClass(avatarClass);
            $('#' + idObjects.chatMainWindow).addClass(agentData.options.avatar);
        };
        
        /* Here we'll add the requiered external scripts */
        this.addScripts = function() {
            $.chatBot.debug('- Start addScripts');
            $.chatBot.debug('Adding Youtube Script.');
            /* Youtube API */
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            $.chatBot.debug('- End addScripts');
        };
        
        this.addStyleSheet = function() {
            var isIE7 = false;
            var isIE8 = false;
            var _protocol = window.location.protocol;
            isIE7 = ($.chatBot.browser.msie && $.chatBot.browser.version === "7");
            isIE8 = ($.chatBot.browser.msie && $.chatBot.browser.version === "8");

            $.chatBot.debug('addStyleSheet'); 
            
            var sheet = document.createElement('link');
            sheet.setAttribute('rel', 'stylesheet');
            sheet.setAttribute('media', 'screen,print');
            sheet.setAttribute('type', 'text/css');
            sheet.setAttribute('href', _protocol + '//' + $.chatBot.settings.assetsEndPoint + '/css/agentbot.css?v=2&token=' + $.chatBot.settings.conversation_options.token);
            
            $('head').append(sheet);
            
            sheet = document.createElement('link');
            sheet.setAttribute('rel', 'stylesheet');
            sheet.setAttribute('media', 'print');
            sheet.setAttribute('type', 'text/css');
            sheet.setAttribute('href', _protocol + '//' + $.chatBot.settings.assetsEndPoint + '/css/agentbot.printable.css?v=2&token=' + $.chatBot.settings.conversation_options.token);
            $('head').append(sheet);

            if (isIE7) {
                sheet = document.createElement('link');
                sheet.setAttribute('rel', 'stylesheet');
                sheet.setAttribute('media', 'screen');
                sheet.setAttribute('type', 'text/css');
                sheet.setAttribute('href', _protocol + '//' + $.chatBot.settings.assetsEndPoint + '/css/agentbot.ie7.css?v=2&token=' + $.chatBot.settings.conversation_options.token);
                $('head').append(sheet);
            }
            
            /* Fix overlay in IE8 */
            if (isIE8) {
                sheet = document.createElement('link');
                sheet.setAttribute('rel', 'stylesheet');
                sheet.setAttribute('media', 'screen');
                sheet.setAttribute('type', 'text/css');
                sheet.setAttribute('href', _protocol + '//' + $.chatBot.settings.assetsEndPoint + '/css/agentbot.ie8.css?v=2&token=' + $.chatBot.settings.conversation_options.token);
                $('head').append(sheet);
            }

            /* add class to body so we can avoid clash rules. */
            $('body').addClass("agentbot");
        };
        
        this.createObjects = function() {
            $.chatBot.debug('createObjects');

            this.createTab();
            
            this.setupLaunchers();

            /* Create overlay window */
            this.createOverlay();

            /* Create the chat window */
            this.createChatWindow();
            
            this.createAvatarItems();
            
            /* Create the start message */
            if ($.chatBot.settings.startMessage == 1) {
                this.createStartMessage();
            }
        };
        
        this.createTab = function() {
            
            if ($.chatBot.settings.showTab === true) {
                var $tab = $('<a>');
                $tab.attr('id', idObjects.tabLauncher);
                $tab.addClass(classObjects.launcher);
                $tab.css('z-index', $.chatBot.settings.zIndex++);
                $tab.data('shouldHide', true);
                $('body').append($tab);
            }

            $.chatBot.debug('showTab value: ' + $.chatBot.settings.showTab);

            $.chatBot.debug('createTab');
        };
        
        this.createStartMessage = function() {
        /* Muestra un mensaje inicial al cargar la página */
            if ($.chatBot.settings.showTab === true) {
                var $startMessage = $('<div>');
                $startMessage.attr('id', idObjects.startMessage);
                $startMessage.appendTo('body');
                $startMessage.html('<div id="start-message-wrapper"></div>');
            }
        };
        
        this.setupLaunchers = function() {
            
            $.chatBot.debug('- Start setupLaunchers');

            $.each($.chatBot.settings.customLauncher, function(index, launcher) {
                $('#' + launcher.id).data('shouldHide', launcher.shouldHide).addClass(classObjects.launcher);
                $.chatBot.debug('-- #' + launcher.id + 'configured');
            });
           
            $.chatBot.debug('- End setupLaunchers');
        };
        
        this.createOverlay = function() {
            if ($.chatBot.settings.overlay === true) {
                var events = $.chatBot.events;
                var $overlay = $('<div>');
                $overlay.attr('id', idObjects.overlay);
                $overlay.appendTo('body');
                //$overlay.on(events.openChatWindow, overlayHandler); /*EVENT*/
            }

            $.chatBot.debug('createOverlay value: ' + $.chatBot.settings.overlay);
        };
        
        this.createAvatarItems = function() {
            // Add available avatar items to the list
            var _protocol = window.location.protocol;
            var html = '';
            $.each(chatAvatars, function(i) {
                var image = _protocol + '//' + $.chatBot.settings.assetsEndPoint + '/image/' + chatAvatars[i].options.avatarClass + '.png';
                html += '<div class="agentbot-avatar-item" data-id="' + i + '" id="avatar-' + i + '"><img src="' + image + '" /></div>';
            });
            $('#' + idObjects.chatAvatarContainerScroll).append($(html));
        };
        
        this.createChatWindow = function() {
            $.chatBot.debug('createCarousel');
            var imagePath = window.location.protocol + '//' +
                $.chatBot.settings.assetsEndPoint + '/image/';
            var avatarList =
                    '<div id="agentbot-avatar-list2">' +
                        '<div id="agentbot-avatar-prev"></div>' +
                        '<div id="carousel">' +
                            '<a href="#"><img src="' + imagePath + 'nico.png" id="item-1" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'lau.png" id="item-2" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'silvia.png" id="item-3" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'jorge.png" id="item-4" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'sebastian.png" id="item-5" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'marta.png" id="item-6" /></a>' +
                            '<a href="#"><img src="' + imagePath + 'alex.png" id="item-7" /></a>' +
                        '</div>' +
                        '<div id="agentbot-avatar-next"></div>' +
                    '</div>';
            
            var Hora = new Date();
            Hora.setTime(Hora.getTime() + (2*60*60*1000));
            
            var classMainWindow = ($.chatBot.settings.position.length > 0) ? ' class="' + $.chatBot.settings.position.join(" ") + '"' : '';
            var html = '<div id="' + idObjects.chatOverlay + '" style="z-index: ' + $.chatBot.settings.zIndex++ + '"></div><div id="' + idObjects.chatMainWindow + '"  style="display:none; z-index: ' + ($.chatBot.settings.zIndex+5) + '" ' + classMainWindow + '>' +
                    '<div id="' + idObjects.chatConversationWrapper + '">' +
                        '<div id="' + idObjects.chatHeader + '">' +
                            '<div id="' + idObjects.chatAvatarList + '">' +
                                '<div id="agentbot-avatar-container">' +
                                    '<div id="agentbot-avatar-container-scroll">' +
                                    '</div>' +
                                '</div>' +
                                
                                '<div class="control-prev"></div>' +
                                '<div class="control-next"></div>' +
                            '</div>' +
                            '<div id="' + idObjects.chatAvatarListButton + '"></div>' +

                            '<div id="' + idObjects.chatComplementOpen + '"></div>' +
                            '<div id="' + idObjects.chatBrandLogo + '"></div>' +
                        '</div>' +
                        '<div id="' + idObjects.chatConversation + '">' +
                            '<div id="' + idObjects.chatWelcomeContainer + '">' +
                                '<div id="' + idObjects.chatWelcomeMessage + '"></div>' +
                            '</div>' +
                        '</div>' +
                        '<input id="' + idObjects.chatInput + '" x-webkit-speech lang="es" type="text" autofocus>' +
                        '<div id="' + idObjects.chatSendButton + '" class="' + classObjects.button + '">' + $.chatBot.settings.text.send + '</div>' +
                        '<div id="'+ idObjects.chatFooter+'">' +
                            '<div id="' + idObjects.chatPrintButton + '" title="Imprimir"></div>' +
                            '<div id="' + idObjects.chatSendMailButton +'" title="Enviar por email"></div>' +
                        '</div>' +
                        '<div id="' + idObjects.chatLoading + '" class="loading"></div>' +
                        '<div id="' + idObjects.emailWindow + '" class="' + classObjects.box +'" style="z-index: ' + $.chatBot.settings.zIndex++ + ';">' +
                            '<div class="' + classObjects.boxContainer + '">' + 
                                '<div class="' + classObjects.loadingMore + '"></div>' +
                                '<span class="' + classObjects.close + '"></span>' +
                                '<input type="text" name="email" id="' + idObjects.inputEmail + '" title="' + $.chatBot.settings.text.inputEmail + '" placeHolder="' + $.chatBot.settings.text.inputEmail + '"/>' +
                                '<br>' +
                                '<label class="error">Ingrese un email v&aacute;lido.</label>' +
                                '<a href="javascript:void(0)" class="' + classObjects.button + '" id="' + idObjects.sendMailButton + '">' + $.chatBot.settings.text.sendMailLabel + '</a>' +
                            '</div>' +
                        '</div>' +
                    '</div>'+
                    '<div id="' + idObjects.chatComplement + '">' + 
                        '<div id="' + idObjects.chatComplementHeader + '">' +
                            /* '<div id="' + idObjects.chatComplementClose + '"></div>' + */
                        '</div>' +
                        '<div id="' + idObjects.chatComplementContainer + '"></div>' +
                        '<div id="' + idObjects.chatFeedback + '">' +
                            '<div class="feedback-col1">' +
                            '<span>Ayudanos a mejorar:</span>' +
                            '<span></span>' +
                            '</div>' +
                            '<div class="feedback-col2">' +
                                '<input id="agentbot-feedback-email" type="text" placeholder="Ingres&aacute; tu e-mail para enviarte una encuesta"/>' +
                                '<div id="agentbot-feedback-send"></div>' +
                            '</div>' +
                            '<div id="agentbot-feedback-thanks">Gracias por tu ayuda</div>' +
                        '</div>' +
                        '<div id="' + idObjects.chatFooter + '"></div>' +
                    '</div>' +
                    '<div id="tab">' +
                        '<div id="' + idObjects.chatCloseButton + '"></div>' +
                    '</div>' +
                '</div>';
            if ($.chatBot.settings.embed != null) {
                $('#' + $.chatBot.settings.embed).html($(html));
                $('#' + idObjects.chatOverlay).remove();
            } else {
                $this.append($(html));
            }
        };
        
        this.addShareButton = function() {
            if ($('#' + idObjects.chatFbShareButton).length == 0) {
                $.chatBot.facebookShare = $('#agentbot-chat-footer').facebookShare({
                    idObject: idObjects.chatFbShareButton,
                    params: {
                        appID: $.chatBot.settings.facebook.appID,
                        message: $.chatBot.settings.facebook.message,
                        name: $.chatBot.settings.facebook.name,
                        description: $.chatBot.settings.facebook.description,
                        link: $.chatBot.settings.facebook.link,
                        picture: $.chatBot.settings.facebook.picture
                    }
                });
            }
        };
        
        this.setupListeners = function() {
            /* Ajax Loader */
            $(document).ajaxStart(function(){
                $('#' + idObjects.chatLoading).show();
                $.chatBot.debug('ajaxStart ' + idObjects.chatLoading);
            }).ajaxStop(function(){
                $('#' + idObjects.chatLoading).fadeOut();
                $.chatBot.debug('ajaxStop');
            });
            
            /* Launchers */
            $(document).on($.chatBot.events.click, '.' + classObjects.launcher, this.eventsHandler.openChatWindow);
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatCloseButton, this.eventsHandler.closeChatWindow);
            
            /* Close Complement */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatComplementClose, this.eventsHandler.closeChatComplement);
            /* Open Complement */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatComplementOpen, this.eventsHandler.openChatComplement);
            
            /* Send Messages */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.keypress, '#' + idObjects.chatInput, this.eventsHandler.keypressChatHandler);
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatSendButton, this.eventsHandler.clickChatHandler);
            /* Write wrapper */
            $('#' + idObjects.chatConversation).on($.chatBot.events.click, 'a[onclick]', this.eventsHandler.writeClickHandler);
            
            /* Print Conversation */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatPrintButton, this.eventsHandler.clickPrintHandler);
            
            /* Send Conversation by Email */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatSendMailButton, this.eventsHandler.clickSendMailHandler);
            $('#' + idObjects.emailWindow).on($.chatBot.events.click, '.' + classObjects.close , this.eventsHandler.clickCancelSendMailHandler);
            $('#' + idObjects.emailWindow).on($.chatBot.events.click, '#' + idObjects.sendMailButton , this.eventsHandler.clickSendMailButtonHandler);
            /* SOLO GOBIERNO DE BUENOS AIRES */
            /* Validate Login Options */
            $('#' + idObjects.loginNameInput).on($.chatBot.events.keypress, this.eventsHandler.loginInputsKeypressHandler);
            $('#' + idObjects.loginEmailInput).on($.chatBot.events.keypress, this.eventsHandler.loginInputsKeypressHandler);
            $('#' + idObjects.loginDoneButton).on($.chatBot.events.click, this.eventsHandler.loginClickHandler);
            
            /* Avatar list */
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '#' + idObjects.chatAvatarListButton, this.eventsHandler.toggleAvatarList);
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '.' + classObjects.avatarItem, this.eventsHandler.toggleAvatarList);
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '.control-prev', this.eventsHandler.prevAvatar);
            $('#' + idObjects.chatMainWindow).on($.chatBot.events.click, '.control-next', this.eventsHandler.nextAvatar);
            $('#' + idObjects.chatFeedback).on($.chatBot.events.click, '#' + idObjects.chatFeedbackSend, this.eventsHandler.sendFeedbackEmail);

//            $('#' + idObjects.chatFeedback).on($.chatBot.events.focus, '#' + idObjects.chatFeedbackEmail, this.eventsHandler.expandFeedbackEmailInput);
//            $('#' + idObjects.chatFeedback).on($.chatBot.events.blur, '#' + idObjects.chatFeedbackEmail, this.eventsHandler.collapseFeedbackEmailInput);
    
            if ($.chatBot.settings.embed == null) {
                $('#' + idObjects.chatHeader).on('mousedown', this.eventsHandler.startDragging);
            }
            
            if ($.chatBot.settings.startMessage == 1) {
                $('#' + idObjects.tabLauncher).on('mouseover', this.eventsHandler.openStartMessage);
                $('#' + idObjects.startMessage).on('click', this.eventsHandler.openChatWindow);
            }
            
            if ($.chatBot.mobile.any) {
                $('#tab').on('click', this.eventsHandler.closeChatWindow);
            }

            /* Always Visible*/
            if($.chatBot.settings.alwaysVisible == true){
                $('#' + idObjects.chatCloseButton).remove();
            }
            $(document).on('mouseup', this.eventsHandler.stopDragging);
            
            
        };
        
        this.centerAvatarItem = function() {
            /* Show item when is partialy or full hide */
            var id =  $.chatBot.settings.avatar;
            var $avatarItems = $('.agentbot-avatar-item');

            $.chatBot.debug('avatar selected');
            $.chatBot.debug(id);
            
            for (var i= 0; i < 7; i++) {
                if ($('.agentbot-avatar-item:nth-child(2)').attr('data-id') !== id) {
                    $.chatBot.debug('NO IGUAL');
                    var first = $('.agentbot-avatar-item:first');
                    var dataID = first.attr('data-id');
                    var content = '<div id="avatar-' + dataID + '" class="agentbot-avatar-item" data-id="' + dataID + '" style="display: block;">' + first.html() + '</div>';
                    first.attr('id', 'to-remove');
                    var $container = $('#' + idObjects.chatAvatarContainerScroll);
                    $container.append(content);
                    $('#to-remove').remove();
                    
                } else {
                    $.chatBot.debug('IGUAL');
                }
                
            }
            $('.agentbot-avatar-item').each(function(index) {
                index < 1 ? $(this).find('img').addClass('left'): $(this).find('img').removeClass('left');
                $(this).find('img').removeClass('center');
                index > 1 ? $(this).find('img').addClass('right'):$(this).find('img').removeClass('right');
                index > 2 ? $(this).find('img').css('visibility','hidden') : $(this).find('img').css('visibility','visible');
            });
            $(".agentbot-avatar-item:nth-child(2)").find('img').addClass('center');
            $(".agentbot-avatar-item:nth-child(2)").addClass('selected');
            
            $.chatBot.debug('centerAvatarItem');
        };
        
        this.changeAvatarItem = function(centerIndex) {
            $.chatBot.debug($(this).attr('data-id'));

            var id = $('.agentbot-avatar-item:nth-child(' + centerIndex + ')').attr('data-id');
            var agentData = '';
            var avatarClass = $.chatBot.settings.avatar;
            var index = 0;
            var avatarIndex = 0;
            var left = 0;

            $('#avatar-' + id).addClass('selected');

            $.each(chatAvatars, function(i) {
                $.chatBot.debug(chatAvatars[i].options.avatarClass);
                index++;
                if (chatAvatars[i].options.avatarClass === id) {
                    agentData = chatAvatars[i];
                    avatarIndex = index;
                    /* Update facebook picture */
                    $.chatBot.settings.facebook.picture = $.chatBot.settings.assetsEndPoint + '/image/' + chatAvatars[i].options.avatarClass + '.png';
                } else {
                    $('#avatar-' + i).removeClass('selected');
                }
            });

            var maxWidth = parseInt($('#' + idObjects.chatAvatarList).css('width')) - 62;
            var itemWidth = parseInt($('.' + classObjects.avatarItem).css('width')) + 
                parseInt($('.' + classObjects.avatarItem).css('padding-left')) * 2;

            left = -13 + ((avatarIndex -5) * - itemWidth);
            if (left > maxWidth) {
                left = 0;
            } else {
                left = 0;
            }

            /* Update bot data */
            $.chatBot.settings.text.agent = agentData.options.name;
            $.chatBot.settings.avatar = agentData.options.avatarClass;
            $.chatBot.settings.text.welcomeMessage = agentData.options.welcomeMessage;
            $.chatBot.settings.text.escuelasMessage = agentData.options.escuelasMessage;
            $.chatBot.settings.text.derivation = agentData.derivation;

            /* Update bot cookies */
            $.chatBot.utils.saveCookie($.chatBot.cookieKeys.avatar, agentData.options);

            /* Remove welcome message */
            $('#' + idObjects.chatWelcomeContainer).remove();

            var $conversation = $('#' + idObjects.chatConversation);
            var welcome = 
                '<div id="' + idObjects.chatWelcomeContainer + '">' +
                    '<div id="' + idObjects.chatWelcomeMessage + '">' + agentData.options.welcomeMessage + '</div>' +
                    '<ul>' +
                    '</ul>' +
            '</div>';

            $('#' + idObjects.chatConversation).append(welcome);

            $.chatBot.utils.scrollDown($conversation);
            $('#' + idObjects.chatMainWindow).find('#' + idObjects.chatInput).focus();

            /* Change logo */
            $('#' + idObjects.chatMainWindow).removeClass(avatarClass);
            $('#' + idObjects.chatMainWindow).addClass(id);
        };
        
        this.eventsHandler = {
            startDragging: function(e) {
                
                $.chatBot.debug('Start Dragging');

                self.dragging = true;
                
                /* Bind Mouse Event */
                $(document).bind($.chatBot.events.mouseMove, self.eventsHandler.moveWindow); //self.eventsHandler.moveWindow

                var $mainWindow = $('#' + idObjects.chatMainWindow);

                var top = ($mainWindow.css('top') === 'auto') ? ($(window).height() - $mainWindow.height()) : $mainWindow.css('top');
                var left = ($mainWindow.css('left') === 'auto') ? ($(window).width() - $mainWindow.width()) : $mainWindow.css('left');
                
                $('#' + idObjects.chatMainWindow).data('position', {
                    x: parseInt(left),
                    y: parseInt(top),
                    offsetY: e.clientY - parseInt(top),
                    offsetX: e.pageX - parseInt(left),
                    width: $('#' + idObjects.chatMainWindow).width(),
                    height: $('#' + idObjects.chatMainWindow).height()
                });

                /* Add notSelectable class to avoid text selection while dragging */
                $('body').addClass('notSelectable');
            },
            moveWindow: function(e) {
            
                setTimeout(function(){

                    var $mainWindow = $('#' + idObjects.chatMainWindow);
                    var position = $mainWindow.data('position');

                    var newPosition = {
                        y: e.clientY - position.offsetY,
                        x: e.clientX - position.offsetX
                    };
                    
                    var outBounds = {
                        x: position.width + newPosition.x,
                        y: position.height + newPosition.y
                    };

                    if (newPosition.x >= 0 && outBounds.x <= $(document).width()) {
                        $mainWindow.css('left', newPosition.x);
                    };

                    if (newPosition.y >= 0 && outBounds.y <= $(window).height()) {
                        $mainWindow.css('top', newPosition.y);
                    };
                }, 25, e);
            },
            stopDragging: function(e) {
                if (self.dragging === true) {
                    $.chatBot.debug('Stop Dragging');
                    /* Unbind Mouse Event */
                    $(document).unbind($.chatBot.events.mouseMove);
                    /* Remove notSelectable class to allow text selection */
                    $('body').removeClass('notSelectable');
                    self.dragging = false;
                }
            },
            prevAvatar: function() {
                $.chatBot.debug('prevAvatar');
                /* Move avatar list to the right */
                if ($('#to-remove').length === 0) {
                    var $container = $('#' + idObjects.chatAvatarContainerScroll);
                    $.chatBot.debug('prevAvatar');                  
                    
                    var last = $('.agentbot-avatar-item:last');
                    var left = last.css('left');
                    var id = last.attr('id');
                    var dataID = last.attr('data-id');
                    var selected = last.hasClass('selected') ? ' selected' : '';
                    
                    /* Left item */
                    var content = '<div id="' + id + '" class="agentbot-avatar-item' + selected + '" data-id="' + dataID + '" style="display: block; width: 0;">' + last.html() + '</div>';

                    $('#' + id).attr('id', 'to-remove');
                    
                    /* Add to left */
                    $container.prepend(content);
                    
                    left = $('#' + id + ' .left').find('img').css('left');
                    $('#' + id).find('img').animate({'left': left},
                        {
                            duration: 300,
                            complete: function() {
                                $('#to-remove').fadeOut(function() {
                                    $('#to-remove').remove();
                                });
                            },
                            start: function() {
                                /* Width left item */
                                $('#' + id).animate({'width': 80},
                                    {
                                        duration: 200
                                    }
                                );
                            }
                        }
                    );
            
                    $('.agentbot-avatar-item').each(function(index) {
                        index < 1 ? $(this).find('img').addClass('left'): $(this).find('img').removeClass('left');
                        $(this).find('img').removeClass('center');
                        index > 1 ? $(this).find('img').addClass('right'):$(this).find('img').removeClass('right');
                        
                        index > 2 ? $(this).find('img').css('visibility','hidden') : $(this).find('img').css('visibility','visible');
                    });

                    $(".agentbot-avatar-item:nth-child(2)").find('img').addClass('center');

                    $(".agentbot-avatar-item:nth-child(2)").trigger('click');
                }
                
                self.changeAvatarItem(2);
            },
            nextAvatar: function() {
                /* Move avatar list to the left */
                if ($('#to-remove').length === 0) {
                    var $container = $('#' + idObjects.chatAvatarContainerScroll);

                    $.chatBot.debug('nextAvatar');

                    var first = $('.agentbot-avatar-item:first');
                    var id = first.attr('id');
                    var dataID = first.attr('data-id');
                    var selected = first.hasClass('selected') ? ' selected' : '';
                    var content = '<div id="' + id + '" class="agentbot-avatar-item' + selected + '" data-id="' + dataID + '" style="display: none;">' + first.html() + '</div>';

                    $('#' + id).attr('id', 'to-remove');
                    $container.append(content);

                    $('#to-remove').find('img').animate({'left': -72},
                        {
                            duration: 310,
                            complete: function() {
                                $('#to-remove').remove();
                                $('.agentbot-avatar-item:nth-child(3)').find('img').css('visibility','visible');
                            },
                            start: function() {
                                $('#to-remove').animate({'width': 0}, 300);
                                $('#' + id).fadeIn(300);
                            }
                        }
                    );
                }
                
                $('.agentbot-avatar-item').each(function(index) {
                    index < 2 ? $(this).find('img').addClass('left'): $(this).find('img').removeClass('left');
                    $(this).find('img').removeClass('center');
                    index > 2 ? $(this).find('img').addClass('right'):$(this).find('img').removeClass('right');
                    index > 3 ? $(this).find('img').css('visibility','hidden') : $(this).find('img').css('visibility','visible');
                });
                
                $('.agentbot-avatar-item:nth-child(4)').find('img').css('visibility','hidden');
                
                $(".agentbot-avatar-item:nth-child(3)").find('img').addClass('center');
                            
                $.chatBot.debug($(".agentbot-avatar-item .center").attr('id'));
                
                self.changeAvatarItem(3);
            },
            toggleAvatarList: function() {
                /* Toggle open/close avatar list */
                if ($('#' + idObjects.chatAvatarContainer).is(':visible')) {
                    // Close avatar list
                } else {
                    // Open avatar list
                    $('.' + classObjects.avatarItem).show();
                    var maxWidth = parseInt($('#' + idObjects.chatAvatarList).css('max-width'));
                    $('#' + idObjects.chatAvatarContainer).show();
                    $('#' + idObjects.chatMainWindow + ' .control-prev').show();
                    $('#' + idObjects.chatAvatarList).animate({'width': maxWidth}, function() {
                        $('#' + idObjects.chatMainWindow + ' .control-next').show();
                    });
                    $('#' + idObjects.chatAvatarListButton).animate({'right': -11});
                    
                    /* Avatar center */
                    $('.agentbot-avatar-item').each(function(index) {
                    index < 1 ? $(this).find('img').addClass('left'): $(this).find('img').removeClass('left');
                    $(this).find('img').removeClass('center');
                        index > 1 ? $(this).find('img').addClass('right'):$(this).find('img').removeClass('right');
                    });
                    $(".agentbot-avatar-item:nth-child(2)").find('img').addClass('center');
                }
            },
            scrollAvatar: function() {
            },
            changeAvatar: function() {
                $.chatBot.debug($(this).attr('data-id'));
                
                var id = $(this).attr('data-id');
                //var id = dataID;
                var agentData = '';
                var avatarClass = $.chatBot.settings.avatar;
                var index = 0;
                var avatarIndex = 0;
                var left = 0;

                $('#avatar-' + id).addClass('selected');
                
                $.each(chatAvatars, function(i) {
                    $.chatBot.debug(chatAvatars[i].options.avatarClass);
                    index++;
                    if (chatAvatars[i].options.avatarClass === id) {
                        agentData = chatAvatars[i];
                        avatarIndex = index;
                        /* Update facebook picture */
                        $.chatBot.settings.facebook.picture = $.chatBot.settings.assetsEndPoint + '/image/' + chatAvatars[i].options.avatarClass + '.png';
                    } else {
                        $('#avatar-' + i).removeClass('selected');
                    }
                });
                
                var maxWidth = parseInt($('#' + idObjects.chatAvatarList).css('width')) - 62;
                var itemWidth = parseInt($('.' + classObjects.avatarItem).css('width')) + 
                    parseInt($('.' + classObjects.avatarItem).css('padding-left')) * 2;
                
                left = -13 + ((avatarIndex -5) * - itemWidth);
                if (left > maxWidth) {
                    left = 0;
                } else {
                    left = 0;
                }

                /* Update bot data */
                $.chatBot.settings.text.agent = agentData.options.name;
                $.chatBot.settings.avatar = agentData.options.avatarClass;
                $.chatBot.settings.text.welcomeMessage = agentData.options.welcomeMessage;
                $.chatBot.settings.text.escuelasMessage = agentData.options.escuelasMessage;
                $.chatBot.settings.text.derivation = agentData.derivation;

                /* Update bot cookies */
                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.avatar, agentData.options);
                
                /* Remove welcome message */
                $('#' + idObjects.chatWelcomeContainer).remove();
                
                var $conversation = $('#' + idObjects.chatConversation);
                var welcome = 
                    '<div id="' + idObjects.chatWelcomeContainer + '">' +
                        '<div id="' + idObjects.chatWelcomeMessage + '">' + agentData.options.welcomeMessage + '</div>' +
                        '<ul>' +
                        '</ul>' +
                '</div>';
                
                $('#' + idObjects.chatConversation).append(welcome);
  
                $.chatBot.utils.scrollDown($conversation);
                $('#' + idObjects.chatMainWindow).find('#' + idObjects.chatInput).focus();

                /* Change logo */
                $('#' + idObjects.chatMainWindow).removeClass(avatarClass);
                $('#' + idObjects.chatMainWindow).addClass(id);
            },
            openLoginWindow: function() {
                /* This method will open the chat window */
                var $mainOverlay = $('#' + idObjects.chatOverlay);
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                var $loginWindow = $('#' + idObjects.loginOptionWindow);
                var $chatComplement = $('#' + idObjects.chatComplement);
                var $tab = $('#' + idObjects.tabLauncher);
                var chatMainMaxWidth = parseInt($mainWindow.css('max-width'));
                var chatMainMinWidth = parseInt($mainWindow.css('min-width'));
                //var lastConversation = null;
                
                /* Check if window is hidden */
                //if (!$mainWindow.is(':visible'))
                /* Stop startMessage */
                if ($.chatBot.settings.startMessage == 1) {
                    clearInterval(intervalHandler);
                    $('#' + idObjects.startMessage).hide();
                }

                $tab.css('z-index', 10002);
                $chatComplement.hide();

                $mainOverlay.fadeIn();

                $mainWindow.css('top', topMax);
                $('#agentbot-tab').css('top', topMax);

                /* Fix mobile */
                if ($.chatBot.mobile.any) {
                    $mainWindow.css('top', 0);
                    $mainWindow.css('position', 'absolute');
                }

                /* Fix embedded */
                if ($.chatBot.settings.embed !== 'chat-embebbed') {
                    $mainWindow.css('left', (chatMainMaxWidth * - 1) - 44);
                } else {
                    $mainWindow.css('top', 0);
                }
                $mainWindow.show();

                $.chatBot.utils.scrollDown($mainWindow.find('#' + idObjects.chatConversation));

                $mainWindow.animate({width: chatMainMinWidth}, 100);
                $('#' + idObjects.loginNameInput).focus();
                $mainWindow.animate({left: 0}, tabSpeed);

                $tab.hide();
                $loginWindow.fadeIn();

                /* set new hash */
                $.chatBot.settings.conversation_options.hash = Math.floor((Math.random()*1000000)+1);
                
                /* clear conversation history */
                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.conversation, null);
                $('#agentbot-chat-conversation .message').html('');
            },
            openChatWindow: function() {
                /* This method will open the chat window */
                var $mainOverlay = $('#' + idObjects.chatOverlay);
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                var $chatComplement = $('#' + idObjects.chatComplement);
                var $tab = $('#' + idObjects.tabLauncher);
                var chatMainMaxWidth = parseInt($mainWindow.css('max-width'));
                
                /* Check if window is hidden */
                if (!$mainWindow.is(':visible')) {
                    /* Stop startMessage */
                    if ($.chatBot.settings.startMessage == 1) {
                        clearInterval(intervalHandler);
                        $('#' + idObjects.startMessage).hide();
                    }

                    $tab.css('z-index', 10002);
                    $chatComplement.hide();

                    $mainOverlay.fadeIn();
                    self.addShareButton();

                    $mainWindow.css('top', topMax);
                    $('#agentbot-tab').css('top', topMax);

                    /* Fix mobile */
                    if ($.chatBot.mobile.any) {
                        $mainWindow.css('top', 0);
                        $mainWindow.css('position', 'absolute');
                    }

                    /* Fix embedded */
                    if ($.chatBot.settings.embed !== 'chat-embebbed') {
                        $mainWindow.css('left', (chatMainMaxWidth * - 1) - 44);
                    } else {
                        $mainWindow.css('top', 0);
                    }
                    $mainWindow.show();

                    $.chatBot.utils.scrollDown($mainWindow.find('#' + idObjects.chatConversation));

                    $mainWindow.animate({width: chatMainMaxWidth}, tabSpeed, function(){
                        $chatComplement.fadeIn(100);
                        $mainWindow.animate({left: 0}, tabSpeed);
                    });
                    $mainWindow.find('#' + idObjects.chatInput).focus();
                    self.complements.showFaqsInit();
                    $tab.hide();
                }
            },
            closeChatWindow: function() {
                /* This method will close the chat window */
                var $mainOverlay = $('#' + idObjects.chatOverlay);
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                var chatMainMaxWidth = parseInt($mainWindow.css('max-width'));
                $('#' + idObjects.chatMainWindow).animate({top: (topMax)}, 100).animate({left: (chatMainMaxWidth * -1)}, tabSpeed, function(){
                    
                    $('#' + idObjects.chatMainWindow).hide();

                    $mainOverlay.fadeOut();
                    /* Trigger closewindow event */
                    $('#' + idObjects.chatComplementContainer).trigger($.chatBot.events.closeChatWindow);
                    $('.' + classObjects.launcher).trigger($.chatBot.events.closeChatWindow);
                    $('#' + idObjects.tabLauncher).show();

                    /* Restore startMessage */
                    if ($.chatBot.settings.startMessage == 1) {
                        intervalHandler = setInterval(function(){self.eventsHandler.openStartMessageLoop()}, dataMessage.interval);
                    }
                    
                });
            },
            openStartMessageLoop: function() {
                $('#' + idObjects.startMessage).css('top', topMax);
                
                if ($('#' + idObjects.startMessage).is(':visible')) {
                    $('#' + idObjects.startMessage).fadeOut(100);
                    clearInterval(intervalHandler);
                    intervalHandler = setInterval(function(){self.eventsHandler.openStartMessageLoop()}, dataMessage.interval);
                } else {
                    if (countRepeats > 0 || dataMessage.numberRepeats == 0) {
                        $('#' + idObjects.startMessage).fadeIn(500);
                        clearInterval(intervalHandler);
                        intervalHandler = setInterval(function(){self.eventsHandler.openStartMessageLoop()}, dataMessage.showTime);
                        
                        if (dataMessage.numberRepeats != 0) countRepeats--;
                    } else {
                        clearInterval(intervalHandler);
                    }
                }
            },
            openStartMessage: function () {
                if (!$('#' + idObjects.startMessage).is(':visible')) {
                    $('#' + idObjects.startMessage).css('top', topMax);
                    clearInterval(intervalHandler);
                    $('#' + idObjects.startMessage).fadeIn(500).delay(5000).fadeOut(200);
                }
            },
            closeChatComplement: function() {
                $.chatBot.utils.collapseComplement();
                $('#' + idObjects.chatComplementOpen).show()
                /* @TODO: Remove class from main window */
                
                /* Trigger closewindow event */
                $('#' + idObjects.chatComplementContainer).trigger($.chatBot.events.closeChatWindow);
            },
            openChatComplement: function() {
                $.chatBot.utils.expandComplement();
                $('#' + idObjects.chatComplementOpen).hide()
                /* $('#' + idObjects.chatComplementOpen).show() */
                /* @TODO: Remove class from main window */
                
                /* Trigger closewindow event */
                $('#' + idObjects.chatComplementContainer).trigger($.chatBot.events.closeChatWindow);
            },
            loginInputsKeypressHandler: function(e) {
                /* Capture enter */
                if (e.which === 13) {
                    e.preventDefault();
                    $('#' + idObjects.loginDoneButton).trigger($.chatBot.events.click);
                }
            },
            keypressChatHandler: function(e) {
                /* This method will handle the key input, and it will send a message to the server if a chr(13) is detected */
                $this = $(this);
                
                /* Capture enter */
                if (e.which === 13) {
                    e.preventDefault();
                    self.processUserMessage($this);
                }
            },
            clickChatHandler: function(e) {
                /* This method will send a message to the server */
                var $inputBox = $('#' + idObjects.chatInput);
                
                self.processUserMessage($inputBox);
            },
            clickFaqInitHandler: function(e) {
                self.complements.showFaqsInit();
            },
            clickOpenUrlHandler: function(e) {
                $.chatBot.debug('clickOpenUrlHandler');
                self.complements.openUrl(e);
            },
            clickPrintHandler: function(e) {
                /* This method will print the conversation */
                $('body').addClass('print');
                
                window.print();
                /* Remove print class and scrollDown after close the print dialog */
                setTimeout(function(){ 
                    $('body').removeClass('print');
                    /* scrollDown */
                    $.chatBot.utils.scrollDown($('#' + idObjects.chatConversation));
                }, 100);
            },
            clickSendMailHandler: function(e) {
                /* This handler will show up the email request modal */
                self.showEmailRequest();
            },
            clickCancelSendMailHandler: function(e) {
                /* This method will close the email request modal */
                self.closeEmailRequest();
            },
            clickSendMailButtonHandler: function(e) {
                var email = $('#' + idObjects.inputEmail).val();
                if ($.chatBot.utils.isValidEmail(email)) {
                    var hash = $.chatBot.settings.conversation_options.hash;
                    $('#' + idObjects.emailWindow).find('label.error').hide();
                    $('#' + idObjects.inputEmail).removeClass('error');
                    $('#' + idObjects.emailWindow).find('.' + classObjects.loadingMore).show();
                    
                    self.sendConversationByEmail(email, hash);                    
                } else {
                    $('#' + idObjects.emailWindow).find('label.error').show();
                    $('#' + idObjects.inputEmail).addClass('error');
                }
            },
            keydownAllowNumbers: function(e) {
                /* Allow: backspace, delete, tab, escape, and enter */
                if ( e.keyCode === 46 || e.keyCode === 8 || e.keyCode === 9 || e.keyCode === 27 || e.keyCode === 13 || 
                     /* Allow: Ctrl+A */
                    (e.keyCode === 65 && e.ctrlKey === true) || 
                     /* Allow: home, end, left, right */
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         /* let it happen, don't do anything */
                         return;
                }
                else {
                    /* Ensure that it is a number and stop the keypress */
                    if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
                        e.preventDefault(); 
                    }   
                }
            },
            phoneClickHandler: function(e) {
                e.preventDefault();
                var $input = $('#' + idObjects.inputPhone);
                var $label = $('#' + idObjects.requestPhoneWindow).find('label.error');
                var $window = $('#' + idObjects.requestPhoneWindow);
                if ($input.val().length > 10) {
                    /* HIDE WINDOW AND SAVE PHONE NUMBER */
                    $input.removeClass('error');
                    $label.hide();
                    $window.fadeOut();
                    var phone = {phoneNumber: $input.val()};
                    $.chatBot.utils.saveCookie($.chatBot.cookieKeys.phone, phone);
                } else {
                    /* ERROR */
                    $input.addClass('error');
                    $label.show();
                }
            },
            writeClickHandler: function(e) {
                /* This method will send message automatically */
                e.preventDefault();
                var value = $(this).attr('onclick');
                value = value.replace('Write(', '');
                value = value.replace(')', '');
                value = value.replace(/['"]/g, '');
                value = $.trim(value);
                $('#' + idObjects.chatInput).val(value);
                $('#' + idObjects.chatSendButton).trigger($.chatBot.events.click);
            },
            loginClickHandler: function(e) {
                e.preventDefault();
                
                var $name = $('#' + idObjects.loginNameInput);
                var $email = $('#' + idObjects.loginEmailInput);
                var $chatComplement = $('#' + idObjects.chatComplement);
                var chatMainMaxWidth = $('#' + idObjects.chatMainWindow).css('max-width');
                var expireTime = $.chatBot.settings.text.expire;
                
                if ($name.val() === '') {
                    $name.next('span').show();
                    $name.focus();
                    return false;
                } else {
                    $name.next('span').hide();
                }
                
                var reEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if ($email.val() === '' || !reEmail.test($email.val())) {
                    $email.next('span').show();
                    $email.focus();
                    return false;
                } else {
                    $email.next('span').hide();
                }
                
                /* Session expire */
                expireTime = $.chatBot.utils.addMinutes(new Date(), sessionTime);
                $.chatBot.settings.text.expire = expireTime;
                
                //$loginWindow.fadeOut();
                
                var loginOption = {name: $name.val(), email: $email.val(), expire: expireTime};

                $('#' + idObjects.loginNameInput).focus();
                
                $.chatBot.settings.text.user = loginOption.name;
                
                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.loginOption, loginOption);

                $('#' + idObjects.chatMainWindow).animate({width: chatMainMaxWidth}, tabSpeed);
                $chatComplement.show();
                self.complements.showFaqsInit();
                self.addShareButton();
            },
            welcomeBotClickHandler: function(e) {
                e.preventDefault();
                
                var message = $.chatBot.settings.text.tramiteMessage;
                var timeOfDay = $.chatBot.utils.getTimesOfDay();
                var greeting = $.chatBot.settings.text.conditionalGreeting[timeOfDay];
                message = message.replace('%s', greeting);

                var $conversation = $('#' + idObjects.chatConversation);
                var $element = self.buildMessage(message, 'agent');
                $conversation.append($element);
                $.chatBot.utils.scrollDown($conversation);
                
                $.chatBot.settings.chatType = 1;
                
                $.ajax({
                    url: $.chatBot.settings.endPoint + '/REST/gcba/click.php',
                    type: 'POST',
                    data:{type:'1'},
                    dataType: 'jsonp',
                    contentType: "application/json"
                }).success(function() {
                    $('#'+idObjects.chatWelcomeContainer).remove();
                });
            },
            welcomeChatClickHandler: function(e) {
                e.preventDefault();
                
                self.complements.openChat();

                var message = $.chatBot.settings.text.derivation;
                var $conversation = $('#' + idObjects.chatConversation);
                var $element = self.buildMessage(message, 'agent');
                $conversation.append($element);
                $.chatBot.utils.scrollDown($conversation);
                $.ajax({
                    url: $.chatBot.settings.endPoint + '/REST/gcba/click.php',
                    type: 'POST',
                    data: {type:'2'},
                    dataType: 'jsonp',
                    contentType: "application/json"
                }).success(function() {
                    $('#'+idObjects.chatWelcomeContainer).remove();
                });
            },
            welcomeBot2ClickHandler: function(e) {
                e.preventDefault();
                
                var message = $.chatBot.settings.text.escuelasMessage;
                var timeOfDay = $.chatBot.utils.getTimesOfDay();
                var greeting = $.chatBot.settings.text.conditionalGreeting[timeOfDay];
                message = message.replace('%s', greeting);

                var $conversation = $('#' + idObjects.chatConversation);
                var $element = self.buildMessage(message, 'agent');
                $conversation.append($element);
                $.chatBot.utils.scrollDown($conversation);

                $.chatBot.settings.chatType = 3;
                self.complements.showFaqsInit();

                $.ajax({
                    url: $.chatBot.settings.endPoint + '/REST/gcba/click.php',
                    type: 'POST',
                    data:{type:'3'},
                    dataType: 'jsonp',
                    contentType: "application/json"
                }).success(function() {
                    $('#'+idObjects.chatWelcomeContainer).remove();
                });
            },
            sendFeedbackEmail: function(e) {
                var email = $('#agentbot-feedback-email').val();
                var sessionID = '';
                if ($.cookie($.chatBot.cookieKeys.conversation) !== null) {
                    var lastConversation = $.cookie($.chatBot.cookieKeys.conversation);
                    sessionID = lastConversation.extra.message.split("-")[1];
                }        
                
                $.chatBot.debug(sessionID);
                                
                if (email !== '' && $.chatBot.utils.isValidEmail(email)) {
                    $('#agentbot-feedback-thanks').fadeIn(200);
                    $.ajax({
                        url: $.chatBot.settings.endPoint + '/REST/gcba/feedback.php',
                        //url: 'http://localhost.apibot/REST/gcba/feedback.php',
                        type: 'GET',
                        data: {email: email, sessionID: sessionID},
                        dataType: 'jsonp',
                        //jsonpCallback: '$aivo.core.complements.showFaqsCustom',
                        contentType: "application/json",
                        complete: function(e) {
                            $('#' + idObjects.chatFeedbackEmail).removeClass('error');
                            $('#' + idObjects.chatFeedbackEmail).val('');
                            
                            setTimeout(function(){
                                $('#agentbot-feedback-thanks').fadeOut(300);
                            }, 3000);
                        }

                    });
                } else {
                    $('#agentbot-feedback-email').addClass('error');
                }
            }
            /*
            expandFeedbackEmailInput: function() {
                var maxWidth = parseInt($('#' + idObjects.chatFeedbackEmail).css('max-width'));
                $('#' + idObjects.chatFeedbackEmail).animate({'width': maxWidth}, 200);
                $('#' + idObjects.chatFeedback + ' span:nth-child(2)').fadeOut(200);
            },
            collapseFeedbackEmailInput: function() {
                var minWidth = parseInt($('#' + idObjects.chatFeedbackEmail).css('min-width'));
                $('#' + idObjects.chatFeedbackEmail).animate({'width': minWidth}, 200);
                $('#' + idObjects.chatFeedback + ' span:nth-child(2)').fadeIn(200);
            }*/
        };

        this.complements = {
            openChat: function(){

                var questions = [];
                var lastQuestion = '';
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                for (var i=3; i >= 0; i--) {
                    var text = $('div.message.user').eq($('div.message.user').length - i).text();
                    if (text !== lastQuestion) {
                        questions.push(text);   
                    }
                    lastQuestion = text;
                }
                
                $('#agentbot-avatar-list').fadeOut(200);
                
                $mainWindow.find('#' + idObjects.chatConversationWrapper).openChat({
                    debug: false,
                    url: 'http://baires.agentbot-i.net/demo/olark.php?olark_key={olark_key}&username={name}&email={email}&questions={questions}',
                    height: 456,
                    width: 355,
                    params: {
                        name: '-',//($.cookie($.chatBot.cookieKeys.loginOption).name),encodeURIComponent('ciudadano')
                        email: '-',//$.cookie($.chatBot.cookieKeys.loginOption).email,buenosaires@aivo.co
                        olark_key: '6121-615-10-1095',
                        questions: encodeURIComponent(JSON.stringify(questions))
                    }
                });
                $('.dontLike').fadeOut('slow');
            },
            /* Buenos Aires faqs */
            getFaqsCustom: function() {
                $.ajax({
                    url: $.chatBot.settings.endPoint + '/REST/gcba/faqs.php?limit=10',
                    type: 'GET',
                    dataType: 'jsonp',
                    jsonpCallback: '$aivo.core.complements.showFaqsCustom',
                    contentType: "application/json"
                });
            },
            showFaqsCustom: function(data) {
                var $mainWindow = $('#' + idObjects.chatMainWindow);

                $mainWindow.find('#' + idObjects.chatComplementContainer).showFaqs({
                    inputText: '#' + idObjects.chatInput,
                    wrappers: {
                        faqsWrapper: idObjects.faqsWrapper,
                        faqsContainer: idObjects.faqsContainer,
                        titleContainer: idObjects.chatComplementHeader
                    },
                    data: data,
                    callback: self.eventsHandler.clickChatHandler,
                    back: self.eventsHandler.clickFaqInitHandler
                });
            },
            showFaqsInit: function(data) {
                $.chatBot.debug('showFaqsInit =)');
                var $mainWindow = $('#' + idObjects.chatMainWindow);

                $mainWindow.find('#' + idObjects.chatComplementContainer).showFaqsInit({
                    inputText: '#' + idObjects.chatInput,
                    wrappers: {
                        faqsWrapper: idObjects.faqsWrapper,
                        faqsContainer: idObjects.faqsContainer,
                        titleContainer: idObjects.chatComplementHeader
                    },
                    callback: self.eventsHandler.clickChatHandler,
                    openUrl: self.eventsHandler.clickOpenUrlHandler
                });
            },
            openUrl: function(data) {
                $.chatBot.debug('Open URL =)');
                $.chatBot.debug(data);
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                $mainWindow.find('#' + idObjects.chatComplementContainer).openUrl({
                    debug: $.chatBot.settings.debug,
                    url: data
                });
            },
            showUrl: function(urlToLoad) {
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                $mainWindow.find('#' + idObjects.chatComplementContainer).openUrl({
                    url: urlToLoad,
                    wrappers: {
                        iframeWrapper: idObjects.iframeWrapper,
                        iframeContainer: idObjects.iframeContainer,
                        titleContainer: idObjects.chatComplementHeader
                    },
                    width: 359,
                    height: 410
                });
            },
            showPicZoom: function(urlToLoad,text) {
                $.chatBot.debug('showPicZoom =)');
                var $mainWindow = $('#' + idObjects.chatMainWindow);
                $mainWindow.find('#' + idObjects.chatComplementContainer).showPicZoom({
                    urlImg: urlToLoad,
                    text: text,
                    wrappers: {
                        iframeWrapper: idObjects.iframeWrapper,
                        iframeContainer: idObjects.iframeContainer,
                        titleContainer: idObjects.chatComplementHeader
                    },
                    openUrl: self.eventsHandler.clickOpenUrlHandler,
                    back: self.eventsHandler.clickFaqInitHandler
                });
            }            
        };
        
        this.showEmailRequest = function() {
            $('#' + idObjects.emailWindow).css({visibility: 'visible'});
        };
        
        this.closeEmailRequest = function() {
            $('#' + idObjects.emailWindow).css({visibility: 'hidden'});
            $('#' + idObjects.inputEmail).val('');
            $('#' + idObjects.emailWindow).find('label.error').hide();
            $('#' + idObjects.inputEmail).removeClass('error');
            $('#' + idObjects.emailWindow).find('.' + classObjects.loadingMore).hide();
        };
        /* This method will process the user message and will send to the server  */
        this.processUserMessage = function($inputBox) {
            var message = $.trim($inputBox.val());
            /* stript tags */
            message = message.replace(/(<([^>]+)>)/ig,"");

            $.chatBot.debug('Message: ' + message + ' lenght ' + message.length);

            if (message.length > 0) {   
                $inputBox.disable();
                this.sendMessage(message);
                var $conversation = $('#' + idObjects.chatConversation);
                var $element = this.buildMessage(message, 'user');
                $conversation.append($element);
                $.chatBot.utils.scrollDown($conversation);
            }
            /* Clear input */
            $inputBox.val('');
        };
        
        /* This method will handle the server response */
        this.processAgentMessage = function(response){
            try {
                $.chatBot.utils.stopLoadingRequest();

                var message = response.answer;
                
                $.chatBot.debug('response');
                $.chatBot.debug(response);

                var lastConversation = $.cookie($.chatBot.cookieKeys.conversation);
                    
                if (lastConversation === null) {
                    lastConversation = $.chatBot.settings.conversation_options;
                }
                
                lastConversation.memory = '';
                if (response.memory !== null && response.memory !== '') {
                    lastConversation.memory = response.memory;
                }

                lastConversation.answerID = 0;
                if (response.hasPattern === "1") {
                    lastConversation.answerID = response.answerID;
                }

                lastConversation.extra = response.extra;

                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.conversation, lastConversation);
                /* GOBIERNO DE BUENOS AIRES */
                var loginOption = $.cookie($.chatBot.cookieKeys.loginOption);
                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.loginOption, loginOption);
                
                var $conversation = $('#' + idObjects.chatConversation);
                
                
                /********** EcoBicis **********/
                /* Cambio la imagen del logo */
                if (lastConversation.extra.action === 'EcoBicis') {
                    $('#agentbot-chat-logo').addClass('ecobicis-logo');
                    $('#' + idObjects.chatAvatarList).fadeOut();
                    /* Derivo a Nico */
                    if ($.chatBot.settings.text.agent !== 'Nico') {
                        this.derivateAvatar('Nico');
                    } else {
                        $conversation.append(this.buildMessage('&iexcl;Me encantan las bicis! Son m&aacute;s f&aacute;cil, m&aacute;s sustentables y hasta m&aacute;s r&aacute;pidas que un auto. &iquest;En qu&eacute; puedo ayudarte?', 'agent'));
                    }
                } else {
                    $('#agentbot-chat-logo').removeClass('ecobicis-logo');
                }
                
                $('#agentbot-chat-main').addClass($.chatBot.settings.avatar);
                
                var $element = this.buildMessage(message, 'agent');
                $conversation.append($element);

                /* create qualify content if is not an evasive */
                if (response.answerID !== 0) {
                    $conversation.qualify({
                        debug: $.chatBot.settings.debug,
                        endPoint: $.chatBot.settings.endPoint,
                        qualifyId: response.extra.message
                    });
                }
                
                $.chatBot.utils.scrollDown($conversation);
                
                /* check extra actions */
                if (lastConversation['extra'] !== undefined) {  
                    var $mainWindow = $('#' + idObjects.chatMainWindow);   
                    if (lastConversation.extra.action === 'changeURL') {
                        $mainWindow.changeUrl({
                            url: lastConversation.extra.param,
                            delay: 3000,
                            type: $.chatBot.settings.changeUrlType
                        });
                    }
                    if (lastConversation.extra.action === 'PlayVideo') {
                        $mainWindow.find('#' + idObjects.chatComplementContainer).playVideo({
                            debug: $.chatBot.settings.debug,
                            autoPlay: true,
                            videoCode: lastConversation.extra.param,
                            wrappers: {
                                videoWrapper: idObjects.videoWrapper,
                                videoContainer: idObjects.videoContainer,
                                titleContainer: idObjects.chatComplementHeader
                            },
                            back: self.eventsHandler.clickFaqInitHandler
                        });
                    }
                    if (lastConversation.extra.action === 'OpenChat') {
                        self.complements.openChat();
                    }
                    
                    if (typeof lastConversation.extra['faqs'] !== 'undefined') {
                        $mainWindow.find('#' + idObjects.chatComplementContainer).showFaqs({
                            inputText: '#' + idObjects.chatInput,
                            wrappers: {
                                faqsWrapper: idObjects.faqsWrapper,
                                faqsContainer: idObjects.faqsContainer,
                                titleContainer: idObjects.chatComplementHeader
                            },
                            data: lastConversation.extra.faqs,
                            callback: self.eventsHandler.clickChatHandler,
                            back: self.eventsHandler.clickFaqInitHandler
                        });
                    }
                    $.chatBot.debug(lastConversation.extra.action);
                    if (lastConversation.extra.action === 'ShowPic') {
                        $.chatBot.debug('if ShowPic');
                        $.chatBot.debug(lastConversation.extra.param);
                        var urlToLoad = lastConversation.extra.param;
                        self.complements.showPicZoom(urlToLoad,message);
                    }
                    if (lastConversation.extra.action === 'showUrl') {
                        var urlToLoad = lastConversation.extra.param;
                        self.complements.showUrl(urlToLoad);
                    }
                }

            } catch (e) {
                $.chatBot.debug('Sending message failed.');
            }
            
            /* Enable input text */
            $('#' + idObjects.chatInput).enable();
        };
        
        this.sendConversationByEmail = function(email, hash) {
            $.chatBot.debug('getting messages for current session');

            $.ajax({
                url: 'http://buenosaires.agentbot.net/email/',
                data: { hash: $.chatBot.settings.conversation_options.hash, email: email },
                dataType: 'jsonp',
                contentType: "application/json",
                type: 'GET',
                success: function() {
                    self.closeEmailRequest();
                },
                error: function(e) {
                    $.chatBot.debug('Failed requesting getMessages. Data: ' + e);
                    self.closeEmailRequest();
                }
            });
        };
        
        /* This method will send a message and then will call proccessAgentMessage */
        this.sendMessage = function(msg) {
            $.chatBot.debug('sending message ' + msg);

            var lastConversation = $.cookie($.chatBot.cookieKeys.conversation);
            if (lastConversation === null) {
                lastConversation = $.chatBot.settings.conversation_options;
            }

            lastConversation.sentence = msg;
            
            var message = {
                token: lastConversation.token,
                hash: lastConversation.hash,
                timezone: -3,
                sentence: msg,
                answerID: lastConversation.answerID,
                memory: lastConversation.memory,
                param: JSON.stringify({host: document.location.href})
            };

            $.chatBot.debug('message: ' + message);

            $.ajax({
                url: $.chatBot.settings.endPoint + '/REST/answer/message.php?callback=?',
                data: message,
                type: 'GET',
                jsonpCallback: '$aivo.core.processAgentMessage',
                contentType: "application/json",
                dataType: 'jsonp'
            });
            
            if (!$('#' + idObjects.chatAvatarList).is(':visible')) {
                self.centerAvatarItem();
                $('#' + idObjects.chatAvatarList).fadeIn();
            }

            $.chatBot.utils.startLoadingRequest();

            /* update session time */
            $.chatBot.utils.updateSession();
        };
        
        /* This method will render the messages whether user or agent message */
        this.buildMessage = function(message, userType) {
            var $message = $('<div>');
            $message.addClass('message');
            $message.addClass(userType);

            var $identifier = $('<span>').html($.chatBot.settings.text[userType] + ': ').addClass(userType);

            $message.append($identifier);
            
            $message.append(message);

            return $message;
        };
        
        /* Load previous conversation if exist */
        this.getPreviousMessages = function() {
            $.chatBot.debug('getting messages for current session');

            $.ajax({
                url: $.chatBot.settings.endPoint + '/REST/messages/?',
                data: { hash: $.chatBot.settings.conversation_options.hash },
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback: '$aivo.core.processPreviousMessages',
                contentType: "application/json"
            });

            /* Clean chatstate */
            $.chatBot.utils.removeCookie($.chatBot.cookieKeys.chatstate);
        };

        this.processPreviousMessages = function(data) {
            self.addPreviousMessages(data);

            /* Check if we have to trigger the open window event. */
            if (self.chatState.windowOpen === true) {
                /* Fires openChatWindow event */
                setTimeout(function() { 
                    $('.' + classObjects.launcher).trigger($.chatBot.events.click);
                }, 1000);
            }
        };
        
        /* Add previous messages to the conversation */
        this.addPreviousMessages = function(data) {
            $.chatBot.debug('Adding data');
            var $conversation = $('#' + idObjects.chatConversation);

            $.each(data, function(index, value) {
                var user = self.buildMessage(value.text, 'user');
                var agent = self.buildMessage(value.response, 'agent');
                $conversation.append(user);
                $conversation.append(agent);
            });
            
            $.chatBot.utils.scrollDown($conversation);
        };
        
        this.showAvatarsList =  function() {
            $.chatBot.debug('showAvatarsList');
            var $mainWindow = $('#' + idObjects.chatMainWindow);
            $('#carousel').waterwheelCarousel(
                {
                    flankingItems: 3,
                    separation: 75,
                    horizonOffset: 3,
                    sizeMultiplier: 0.90,
                    movingToCenter: function ($item) {
                        $('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movedToCenter: function ($item) {
                        $('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movingFromCenter: function ($item) {
                        $('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movedFromCenter: function ($item) {
                        $('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
                    },
                    clickedCenter: function ($item) {
                        $('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
                    }
                }
            );
        };
        
        this.init = function() {
            
            $.chatBot.browser.detect();
            $.chatBot.mobile.detect();
            
            /* Setup cookie to serialize data before saving it */
            $.cookie.json = true;
            
            /* Get the max z-index on the document so we can increment by one each element that needs to be in the front */
            $.chatBot.settings.zIndex = $.chatBot.utils.getMaxZindex($('body > *'))+9999;
            
            /* Check if we were redirected from another page */
            
            /* Add scripts, stylesheets and chatwindow */            
            this.addStyleSheet();
            this.addScripts();
            this.createObjects();
            
            /* Check cookies */
            
            /* Set expire from cookie */
            if ($.cookie($.chatBot.cookieKeys.loginOption) !== null) {
                if ($.chatBot.settings.text.expire == '') {
                    $.chatBot.settings.text.expire = $.cookie($.chatBot.cookieKeys.loginOption).expire;
                }
            } else {
                $.chatBot.settings.text.expire = $.chatBot.utils.addMinutesToCurrentTime(sessionTime);
            }
            
            if ($.cookie($.chatBot.cookieKeys.avatar) == null) {
                var avatarOptions = {avatarClass: $.chatBot.settings.avatar,
                    name: $.chatBot.settings.text.agent,
                    welcomeMessage: $.chatBot.settings.text.welcomeMessage,
                    escuelasMessage: $.chatBot.settings.text.escuelasMessage};
                $.chatBot.utils.saveCookie($.chatBot.cookieKeys.avatar, avatarOptions);
            }
            
            $.chatBot.settings.text.agent = $.cookie($.chatBot.cookieKeys.avatar).name;
            $.chatBot.settings.text.escuelasMessage = $.cookie($.chatBot.cookieKeys.avatar).escuelasMessage;
            $.chatBot.settings.avatar = $.cookie($.chatBot.cookieKeys.avatar).avatarClass;
            $('#'+idObjects.chatWelcomeMessage).html($.cookie($.chatBot.cookieKeys.avatar).welcomeMessage);
            $('#'+idObjects.chatMainWindow).addClass($.cookie($.chatBot.cookieKeys.avatar).avatarClass);
            
            $.chatBot.debug($.chatBot.settings.avatar);
            $('#avatar-' + $.chatBot.settings.avatar).addClass('selected');
            
            /* Load conversation cookie if there is any */
            if ($.cookie($.chatBot.cookieKeys.conversation) !== null) {
                $.chatBot.settings.conversation_options = $.extend({}, $.chatBot.settings.conversation_options, $.cookie($.chatBot.cookieKeys.conversation));
            }

            /* Load chatState cookie if there is any */
            if ($.cookie($.chatBot.cookieKeys.chatstate) !== null) {
                self.chatState = $.extend({}, self.chatstate, $.cookie($.chatBot.cookieKeys.chatstate));
                $.chatBot.debug('state');
                $.chatBot.debug(self.chatstate);
                $.chatBot.utils.removeCookie($.chatBot.cookieKeys.chatstate);
            }
            
            /* SOLO GOBIERNO DE BUENOS AIRES */
            /* Check if login options already sent */
            var loginOption = $.cookie($.chatBot.cookieKeys.loginOption);
            if (loginOption !== null) {
                $('#' + idObjects.loginNameInput).val(loginOption.name);
                $('#' + idObjects.loginEmailInput).val(loginOption.email);
                $('#' + idObjects.loginOptionWindow).hide();
                $.chatBot.settings.text.user = loginOption.name;
            }
            
            /* Load previous chat */
            this.getPreviousMessages();
            
            /* Setup listeners */
            this.setupListeners();

            if ($.chatBot.settings.alwaysVisible == true) {
                $.chatBot.debug('openChat');
                setTimeout(function() {
                    self.eventsHandler.openChatWindow();
                    /* $.chatBot.utils.centerInWindow($(this)); */
                    
                    var $mainWindow = $('#' + idObjects.chatMainWindow);
                    /* Check if window is hidden */
                    $mainWindow.fadeIn(100);
                }, 1000);
            }
            
            /* Init start message interval */
            if ($.chatBot.settings.startMessage == 1) {
                intervalHandler = setInterval(function() {
                    self.eventsHandler.openStartMessageLoop();
                }, dataMessage.interval);
            }
            
            $('#' + idObjects.tabLauncher).css('top', topMax);
            $('#' + idObjects.chatMainWindow).css('top', topMax);
            
//            this.showAvatarsList();
            setTimeout(function() {
                self.eventsHandler.toggleAvatarList();
                self.centerAvatarItem();
            }, 1200);

            $.chatBot.debug('End Init -------------------------------');
        };
        
        this.init();
        
        $aivo.core = this;
        
        return this;
    };
    
})($aivo);

/* Para evitar error de javascript */
function Write(val) {}