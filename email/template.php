<body>
<div style ="width: 80%;
		    padding: 15px;
		    margin: 5px auto 0 auto;
		    display: block;
		    background-color: #FFF;
		    overflow: auto;
		    border: 1px solid #7EA7AF;
		    -webkit-box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    box-sizing: border-box;
		    font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
		    font-size: 11px;"
>
	<div id="agentbot-chat-welcome-container">
		<div style="border-bottom: 1px dotted #005C84; color: #005279; padding: 5px 0;color: #005279; font-size: 15px;">
			Asesor Online de Ciudad de Buenos Aires
		</div>
		<?php foreach ($conversation as $key => $value) { ?>
			<div style="color: #666; margin-top: 15px;">
				<span  style="font-weight: bold;">
					Yo: <?php echo utf8_decode($value->text)?>
				</span>
			</div>
			<div style="color: #666; margin-top: 15px; padding: 3px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; background: #F6F6F4;">
				<span style="font-weight: bold;">
					BA: <?php echo utf8_decode($value->response)?>
				<span>
			</div>
		<?php }?>
	</div>
</div>
</body>